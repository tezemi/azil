﻿using System.Collections.Generic;
using MEC;

namespace UnityEngine.UI.Utilities
{
    public static class TransformExtensions
    {
        public static float SetPosition(this Graphic graphic, Vector2 position, float time)
        {
            graphic.enabled = true;

            return Timing.WaitUntilDone(GraphicManager.Lock(graphic, EffectType.SetPosition, SetPositionCoroutine()));

            IEnumerator<float> SetPositionCoroutine()
            {
                var distance = Vector2.Distance(graphic.rectTransform.anchoredPosition, position);

                while (graphic.rectTransform.anchoredPosition != position)
                {
                    yield return Timing.WaitForOneFrame;

                    graphic.rectTransform.anchoredPosition = Vector2.MoveTowards(graphic.rectTransform.anchoredPosition, position, Utils.GetDistanceFromTime(time, distance));
                }

                GraphicManager.Free(graphic, EffectType.SetPosition);
            }
        }

        public static float SetScale(this Graphic graphic, Vector3 scale, float time)
        {
            graphic.enabled = true;

            return Timing.WaitUntilDone(GraphicManager.Lock(graphic, EffectType.SetScale, SetSizeCoroutine()));

            IEnumerator<float> SetSizeCoroutine()
            {
                var distance = Vector3.Distance(graphic.rectTransform.localScale, scale);

                while (graphic.rectTransform.localScale != scale)
                {
                    yield return Timing.WaitForOneFrame;

                    graphic.rectTransform.localScale = Vector3.MoveTowards(graphic.rectTransform.localScale, scale, Utils.GetDistanceFromTime(time, distance));
                }

                GraphicManager.Free(graphic, EffectType.SetScale);
            }
        }
    }
}
