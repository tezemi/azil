using System.Collections.Generic;
using MEC;

namespace UnityEngine.UI.Utilities
{
    public static class FadeExtensions
    {
        public static void Clear(this Graphic graphic)
        {
            var color = graphic.color;
            color.a = 0f;
            graphic.color = color;
        }

        public static CoroutineHandle FadeIn(this Graphic graphic, float time, bool overrideLock = true)
        {
            if (GraphicManager.IsLocked(graphic, EffectType.Fade))
            {
                if (overrideLock)
                {
                    GraphicManager.Free(graphic, EffectType.Fade);
                }
                else
                {
                    return new CoroutineHandle();
                }
            }

            graphic.enabled = true;

            return GraphicManager.Lock(graphic, EffectType.Fade, FadeCoroutine(graphic, graphic.color.RGB(), time));             
        }

        public static CoroutineHandle FadeIn(this Graphic graphic, float time, float maxAlpha)
        {
            if (GraphicManager.IsLocked(graphic, EffectType.Fade))
                return GraphicManager.Cancel();

            graphic.enabled = true;

            return GraphicManager.Lock(graphic, EffectType.Fade, FadeCoroutine(graphic, graphic.color.RGB(maxAlpha), time));
        }

        public static CoroutineHandle FadeOut(this Graphic graphic, float time, bool disableGraphicAtEnd = false, bool overrideLock = false)
        {
            if (GraphicManager.IsLocked(graphic, EffectType.Fade))
            {
                if (overrideLock)
                {
                    GraphicManager.Free(graphic, EffectType.Fade);
                }
                else
                {
                    return GraphicManager.Cancel();
                }
            }            

            graphic.enabled = true;

            if (disableGraphicAtEnd)
            {
                return Timing.RunCoroutine
                (
                    Utils.DisableGraphicAfterCoroutine(FadeCoroutine(graphic, graphic.color.RGB(0f), time), graphic, EffectType.Fade)
                );
            }

            return GraphicManager.Lock(graphic, EffectType.Fade, FadeCoroutine(graphic, graphic.color.RGB(0f), time));
        }

        public static CoroutineHandle FadeColor(this Graphic graphic, float time, Color color)
        {
            if (GraphicManager.IsLocked(graphic, EffectType.Fade))
                return GraphicManager.Cancel();

            graphic.enabled = true;

            return GraphicManager.Lock(graphic, EffectType.Fade, FadeCoroutine(graphic, color, time));
        }

        private static IEnumerator<float> FadeCoroutine(Graphic graphic, Color color, float time)
        {
            while (graphic.color != color)
            {
                yield return Timing.WaitForOneFrame;

                graphic.color = Vector4.MoveTowards(graphic.color, color, Utils.GetDistanceFromTime(time));                
            }

            yield return Timing.WaitForOneFrame;

            GraphicManager.Free(graphic, EffectType.Fade);
        }
    }
}
