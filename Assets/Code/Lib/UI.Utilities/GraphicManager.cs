﻿using System.Collections.Generic;
using MEC;

namespace UnityEngine.UI.Utilities
{
    public static class GraphicManager
    {
        public static readonly Dictionary<(Graphic, EffectType), CoroutineHandle> _lockedGraphics = new Dictionary<(Graphic, EffectType), CoroutineHandle>();

        public static CoroutineHandle Cancel()
        {
            return Timing.RunCoroutine(LockedCoroutine());

            IEnumerator<float> LockedCoroutine()
            {
                yield break;
            }
        }

        public static CoroutineHandle Lock(Graphic graphic, EffectType effectType, IEnumerator<float> coroutine)
        {
            if (_lockedGraphics.ContainsKey((graphic, effectType)))
            {
                Debug.LogWarning($"Trying to lock graphic '{graphic.name}' twice. This should not occur.");

                return new CoroutineHandle();
            }

            var c = Timing.RunCoroutine(coroutine, Segment.FixedUpdate, graphic.name);
            _lockedGraphics.Add((graphic, effectType), c);

            return c;
        }

        public static void Free(Graphic graphic, EffectType effectType)
        {
            if (!_lockedGraphics.ContainsKey((graphic, effectType)))
            {
                Debug.LogWarning($"Trying to free graphic '{graphic.name}' that isn't locked. This should not occur.");

                return;
            }

            Timing.KillCoroutines(_lockedGraphics[(graphic, effectType)]);            

            _lockedGraphics.Remove((graphic, effectType));
        }

        public static bool IsLocked(Graphic graphic, EffectType effectType)
        {
            return _lockedGraphics.ContainsKey((graphic, effectType));
        }
    }
}
