﻿
namespace UnityEngine.UI.Utilities
{
    public static class GraphicExtensions
    {
        public static void CancelEffect(this Graphic graphic, EffectType type)
        {
            GraphicManager.Free(graphic, type);
        }
    }
}
