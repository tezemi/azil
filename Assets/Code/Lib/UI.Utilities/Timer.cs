﻿using System.Collections.Generic;
using MEC;

namespace UnityEngine.UI.Utilities
{
    /// <summary>
    /// Provides utilities that can allow users to create timers which can 
    /// be waited on in coroutines. Timers can be extended to extend the
    /// duration of the wait.
    /// </summary>
    public static class Timer
    {
        private static readonly Dictionary<int, float> _times = new Dictionary<int, float>();
        private static readonly HashSet<int> _elapsedTimes = new HashSet<int>();
        private static readonly HashSet<int> _keysCache = new HashSet<int>();

        static Timer()
        {
            Timing.RunCoroutine(TakeTimeCoroutine(), Segment.FixedUpdate);
        }

        /// <summary>
        /// Creates a new timer with the specified ID. Waits until the 
        /// specified time runs out. You can use 
        /// <see cref="Extend(int, float)"/> to set the time of the
        /// timer with the ID after it has already been created.
        /// </summary>
        /// <param name="id">Unique ID for this timer.</param>
        /// <param name="time">The amount of time to wait.</param>
        /// <returns>Waits until the timer has elapsed.</returns>
        public static float Create(int id, float time)
        {
            if (!_times.ContainsKey(id))
            {
                _keysCache.Add(id);
                _times[id] = time;
            }

            return Timing.WaitUntilDone(new WaitUntil(() => _elapsedTimes.Contains(id)));
        }

        /// <summary>
        /// Sets the duration of a timer with the specified ID.
        /// </summary>
        /// <param name="id">The ID of the timer to extend.</param>
        /// <param name="time">The amount of time to set the tiemr to.
        /// </param>
        public static void Extend(int id, float time)
        {
            _times[id] = time;
            _keysCache.Add(id);
        }

        private static IEnumerator<float> TakeTimeCoroutine()
        {
            while (true)
            {                
                foreach (var key in _keysCache)
                {
                    _times[key] -= Time.fixedDeltaTime;

                    if (_times[key] <= 0f)
                    {
                        _elapsedTimes.Add(key);
                    }
                }

                yield return Timing.WaitForOneFrame;

                foreach (var time in _elapsedTimes)
                {
                    _times.Remove(time);
                    _keysCache.Remove(time);
                }

                _elapsedTimes.Clear();
            }
        }
    }
}
