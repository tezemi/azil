﻿
namespace UnityEngine.UI.Utilities
{
    public enum EffectType
    {
        Fade,
        SetScale,
        SetPosition,
        FontSize
    }
}
