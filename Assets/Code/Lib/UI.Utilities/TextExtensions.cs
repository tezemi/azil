﻿using System.Collections.Generic;
using MEC;

namespace UnityEngine.UI.Utilities
{
    public static class TextExtensions
    {
        public static float SetFontSize(this Text text, int fontSize, float time)
        {
            text.enabled = true;

            return Timing.WaitUntilDone(GraphicManager.Lock(text, EffectType.FontSize, SetFontSizeCoroutine()));

            IEnumerator<float> SetFontSizeCoroutine()
            {
                var distance = Mathf.Abs(text.fontSize - fontSize);

                while (text.fontSize != fontSize)
                {
                    yield return Timing.WaitForOneFrame;

                    var prevFontSize = text.fontSize;
                    Debug.Log($"{text.fontSize} ->");
                    text.fontSize = Mathf.RoundToInt(Mathf.MoveTowards(text.fontSize, fontSize, Utils.GetDistanceFromTime(time, distance)));

                    // Font size didn't change :(
                    if (prevFontSize == text.fontSize)
                    {
                        if (fontSize > text.fontSize)
                        {
                            text.fontSize++;
                        }
                        else
                        {
                            text.fontSize--;
                        }
                    }

                    Debug.Log($"{text.fontSize}");
                }

                GraphicManager.Free(text, EffectType.FontSize);
            }
        }
    }
}
