﻿using UnityEngine;

namespace Azil.Characters.AttackConfigs
{
    [CreateAssetMenu(fileName = nameof(FlurrySpinAttackConfig), menuName = "Azil/" + nameof(FlurrySpinAttackConfig))]
    public class FlurrySpinAttackConfig : AttackConfig
    {
        public GameObject FlurrySpinProjectilePrefab;
    }
}
