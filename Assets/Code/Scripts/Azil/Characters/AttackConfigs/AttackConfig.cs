﻿using UnityEngine;

namespace Azil.Characters.AttackConfigs
{
    [CreateAssetMenu(fileName = nameof(AttackConfig), menuName = "Azil/" + nameof(AttackConfig))]
    public class AttackConfig : ScriptableObject
    {
        public string Name;
        [TextArea]
        public string Description;
        public int SpecialCost;
        #if UNITY_EDITOR
        public UnityEditor.MonoScript AttackScript;
        #endif
        [HideInInspector]
        public string AttackScriptTypeName;

        #if UNITY_EDITOR
        protected virtual void OnValidate()
        {
            if (AttackScript != null)
                AttackScriptTypeName = AttackScript.name;
        }
        #endif
    }
}

