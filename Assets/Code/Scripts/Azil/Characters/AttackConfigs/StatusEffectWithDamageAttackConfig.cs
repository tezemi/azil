﻿using Azil.Utilities;
using Azil.Characters.StatusEffectConfigs;
using UnityEngine;
using MyBox;

namespace Azil.Characters.AttackConfigs
{
    [CreateAssetMenu(fileName = nameof(StatusEffectWithDamageAttackConfig), menuName = "Azil/" + nameof(StatusEffectWithDamageAttackConfig))]
    public class StatusEffectWithDamageAttackConfig : AttackConfig
    {
        [PositiveValueOnly]
        public float Range;
        public GameObject AttackInteractionPrefab;
        public StatusEffectConfig ApplyWhenAccurateStatusEffectConfig;
        public SpriteAnimation WindUpSpriteAnimation;
        public SpriteAnimation SwingSpriteAnimation;
    }
}
