﻿using Azil.Characters.StatusEffectConfigs;
using UnityEngine;

namespace Azil.Characters.AttackConfigs
{
    [CreateAssetMenu(fileName = nameof(RevealEnemyStatusAttackConfig), menuName = "Azil/" + nameof(RevealEnemyStatusAttackConfig))]
    public class RevealEnemyStatusAttackConfig : AttackConfig
    {
        public StatusEffectConfig RevealedStatusEffectConfig;
    }
}
