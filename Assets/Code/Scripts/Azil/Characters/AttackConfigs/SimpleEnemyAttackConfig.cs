﻿using Azil.Utilities;
using UnityEngine;

namespace Azil.Characters.AttackConfigs
{
    [CreateAssetMenu(fileName = nameof(SimpleEnemyAttackConfig), menuName = "Azil/" + nameof(SimpleEnemyAttackConfig))]
    public class SimpleEnemyAttackConfig : AttackConfig
    {
        public SpriteAnimation WindUpSpriteAnimation;
        public SpriteAnimation SwingSpriteAnimation;
    }
}
