﻿using UnityEngine;

namespace Azil.Characters.StatusEffectConfigs
{
    [CreateAssetMenu(fileName = nameof(BurningStatusEffectConfig), menuName = "Azil/" + nameof(BurningStatusEffectConfig))]
    public class BurningStatusEffectConfig : StatusEffectConfig
    {
        [Range(0f, 1f)]
        public float PercentageOfMaxHealthDoneAsDamage;
    }
}

