﻿using UnityEngine;

namespace Azil.Characters.StatusEffectConfigs
{
    [CreateAssetMenu(fileName = nameof(StatusEffectConfig), menuName = "Azil/" + nameof(StatusEffectConfig))]
    public class StatusEffectConfig : ScriptableObject
    {
        public string Name;
        [TextArea]
        public string Description;
        public bool KeepOnDeath;
        public bool KeepAfterBattle;
        public Sprite Icon;
        #if UNITY_EDITOR
        public UnityEditor.MonoScript StatusEffectScript;
        #endif
        [HideInInspector]
        public string StatusEffectTypeName;

        #if UNITY_EDITOR
        protected virtual void OnValidate()
        {
            if (StatusEffectScript != null)
                StatusEffectTypeName = StatusEffectScript.name;
        }
        #endif
    }
}

