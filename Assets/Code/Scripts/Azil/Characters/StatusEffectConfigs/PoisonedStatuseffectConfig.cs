﻿using UnityEngine;

namespace Azil.Characters.StatusEffectConfigs
{
    [CreateAssetMenu(fileName = nameof(PoisonedStatusEffectConfig), menuName = "Azil/" + nameof(PoisonedStatusEffectConfig))]
    public class PoisonedStatusEffectConfig : StatusEffectConfig
    {
        [Range(0f, 1f)]
        public float PercentageOfMaxHealthDoneAsDamage;
    }
}

