﻿using Azil.Utilities; 
using UnityEngine;

namespace Azil.Characters
{
    [CreateAssetMenu(fileName = nameof(CharacterSprites), menuName = "Azil/" + nameof(CharacterSprites))]
    public class CharacterSprites : ScriptableObject
    {
        [Header("Battle Sprites")]
        public SpriteAnimation BattleIdle;
        public SpriteAnimation BattleHurt;
        public SpriteAnimation BattleMoveForward;
        public SpriteAnimation BattleMoveBackward;
        public SpriteAnimation BattleIncapacitate;

        [Header("Overworld Sprites")]
        public Sprite[] Walking;
        public Sprite[] Running;
    }
}
