﻿using UnityEngine;
using Azil.Characters.AttackConfigs;

namespace Azil.Characters
{
    [CreateAssetMenu(fileName = nameof(CharacterBase), menuName = "Azil/" + nameof(CharacterBase))]
    public class CharacterBase : ScriptableObject
    {
        public int StartLevel = 1;
        public float HPModifier = 1f;
        public float SPModifier = 1f;
        public float AttackModifier = 1f;
        public float DefenseModifier = 1f;        
        public float SpeedModifier = 1f;
        public string Name;
        public Sprite Icon;
        public Team Team;
        public CharacterSprites CharactersSprites;
        public AttackConfig[] DefaultAttacks;
    }
}
