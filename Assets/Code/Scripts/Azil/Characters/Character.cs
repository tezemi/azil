﻿using System.Collections.Generic;
using UnityEngine;
using Azil.Characters.AttackConfigs;
using Azil.Characters.StatusEffectConfigs;

namespace Azil.Characters
{
    public class Character
    {
        public int XP { get; set; }
        public int Level { get; set; } = 1;
        public int CurrentHealth { get; set; } = 1;
        public int CurrentSpecial { get; set; } = 1;
        public int HP { get; set; } = 10;
        public int SP { get; set; } = 10;       
        public int Attack { get; set; } = 10;
        public int Defense { get; set; } = 10;
        public int Speed { get; set; } = 10;
        public float HPModifier { get; set; } = 1f;
        public float SPModifier { get; set; } = 1f;
        public float AttackModifier { get; set; } = 1f;
        public float DefenseModifier { get; set; } = 1f;
        public float SpeedModifier { get; set; } = 1f;
        public CharacterBase CharacterBase { get; set; }
        public List<AttackConfig> Attacks { get; } = new List<AttackConfig>();
        public List<StatusEffectConfig> StatusEffects { get; } = new List<StatusEffectConfig>();
        public int MaxHealth => Mathf.RoundToInt(HP * HEALTH_PER_HP);
        public int MaxSpecial => Mathf.RoundToInt(SP * SPECIAL_PER_SP);
        public int XPNeededForLevelUp => 100 + Mathf.RoundToInt(Mathf.Pow(Level, 3f));
        public string Name => CharacterBase.Name;
        public Team Team => CharacterBase.Team;
        private const float HEALTH_PER_HP = 2f;
        private const float SPECIAL_PER_SP = 0.5f;
        private const float STAT_GAINED_PER_LEVEL = 10f;
        private const float ATTACK_DAMAGE_SCALE = 0.5f;
        private const float DEFENSE_DAMAGE_SCALE = 0.25f;

        public Character(CharacterBase characterBase)
        {
            HPModifier = characterBase.HPModifier;
            SPModifier = characterBase.SPModifier;
            AttackModifier = characterBase.AttackModifier;
            DefenseModifier = characterBase.DefenseModifier;            
            SpeedModifier = characterBase.SpeedModifier;
            CharacterBase = characterBase;

            foreach (AttackConfig attack in characterBase.DefaultAttacks)
            {
                Attacks.Add(attack);
            }

            for (int i = 1; i < characterBase.StartLevel; i++)
            { 
                LevelUp();
            }

            CurrentHealth = MaxHealth;
            CurrentSpecial = MaxSpecial;
        }

        public void LevelUp()
        {
            Level++;

            var oldMaxHealth = MaxHealth;

            HP += Mathf.RoundToInt(STAT_GAINED_PER_LEVEL * HPModifier);            
            SP += Mathf.RoundToInt(STAT_GAINED_PER_LEVEL * SPModifier);
            Attack += Mathf.RoundToInt(STAT_GAINED_PER_LEVEL * AttackModifier);
            Defense += Mathf.RoundToInt(STAT_GAINED_PER_LEVEL * DefenseModifier);
            Speed += Mathf.RoundToInt(STAT_GAINED_PER_LEVEL * SpeedModifier);

            var newMaxHealth = MaxHealth;
            var difference = newMaxHealth - oldMaxHealth;

            CurrentHealth += difference;
        }

        public int GetDamageFromAttack(int baseDamage)
        {
            return baseDamage + Mathf.RoundToInt(Attack * ATTACK_DAMAGE_SCALE); 
        }

        public int ReduceDamageFromDefense(int damage)
        {
            return damage - Mathf.RoundToInt(Defense * DEFENSE_DAMAGE_SCALE);
        }
    }
}
