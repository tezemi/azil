﻿using UnityEngine;

namespace Azil.Characters
{
    [CreateAssetMenu(fileName = nameof(Team), menuName = "Azil/" + nameof(Team))]
    public class Team : ScriptableObject
    {
        public bool IsPlayerTeam;
        public string Name;
        public Color Color;
    }
}
