﻿using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Azil.Utilities
{
    public static class PrefabPool
    {
        public static readonly Dictionary<string, Stack<GameObject>> PooledObjects = new Dictionary<string, Stack<GameObject>>();

        public static void Pool(GameObject obj)
        {
            obj.SetActive(false);
            obj.transform.SetParent(null);
            obj.hideFlags = HideFlags.HideInHierarchy;

            if (obj.TryGetComponent(out Rigidbody rigidbody))
            {
                rigidbody.velocity = Vector3.zero;
            }

            var nameWithoutClone = obj.name.Replace("(Clone)", string.Empty);

            if (!PooledObjects.ContainsKey(nameWithoutClone))
            {
                PooledObjects.Add(nameWithoutClone, new Stack<GameObject>());
            }

            PooledObjects[nameWithoutClone].Push(obj);
        }

        public static GameObject Get(GameObject prefab)
        {
            GameObject obj = null;
            if (PooledObjects.ContainsKey(prefab.name) && PooledObjects[prefab.name].Count > 0)
            {
                while (obj == null && PooledObjects[prefab.name].Count > 0)
                {
                    obj = PooledObjects[prefab.name].Pop();
                }

                if (obj != null)
                {
                    obj.SetActive(true);
                    obj.hideFlags = HideFlags.None;
                }
                else
                {
                    obj = Object.Instantiate(prefab);

                    if (obj.name.Contains("(Clone)"))
                    {
                        obj.name = obj.name.Replace("(Clone)", string.Empty);
                    }
                }
            }
            else
            {
                obj = Object.Instantiate(prefab);

                if (obj.name.Contains("(Clone)"))
                {
                    obj.name = obj.name.Replace("(Clone)", string.Empty);
                }
            }

            return obj;
        }
    }
}
