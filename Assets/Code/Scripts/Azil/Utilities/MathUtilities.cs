﻿using UnityEngine;

namespace Azil.Utilities
{
    public static class MathUtilities
    {
        public static float GetRange(float input, float inputLow, float inputHigh, float outputLow, float outputHigh)
        {
            return (input - inputLow) / (inputHigh - inputLow) * (outputHigh - outputLow) + outputLow;
        }

        public static float GetRangeClamped(float input, float inputLow, float inputHigh, float outputLow, float outputHigh)
        {
            var result = GetRange(input, inputLow, inputHigh, outputLow, outputHigh);

            return Mathf.Clamp(result, outputLow, outputHigh);
        }

        public static Vector2 InverseLerp(Vector2 a, Vector2 b, float value)
        {
            return new Vector2(Mathf.InverseLerp(a.x, b.x, value), Mathf.InverseLerp(a.y, b.y, value));
        }
    }
}
