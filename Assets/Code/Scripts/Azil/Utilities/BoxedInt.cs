﻿
namespace Azil.Utilities
{
    public class BoxedInt
    {
        public int Value { get; set; }

        public static implicit operator int (BoxedInt boxedInt)
        {
            return boxedInt.Value;
        }

        public static implicit operator BoxedInt (int value)
        {
            return new BoxedInt { Value = value };
        }
    }
}
