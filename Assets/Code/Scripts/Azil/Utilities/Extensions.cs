﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Azil.Utilities
{
    public static class Extensions
    {
        public static Vector3 Center(this IEnumerable<Vector3> points)
        {
            var center = Vector3.zero;

            foreach (var point in points)
            {
                center += point;
            }

            center = center / points.Count();

            return center;
        }
    }
}
