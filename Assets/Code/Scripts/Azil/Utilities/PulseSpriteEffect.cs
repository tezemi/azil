﻿using System.Collections.Generic;
using UnityEngine;
using MEC;

namespace Azil.Utilities
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class PulseSpriteEffect : MonoBehaviour
    {
        public float Frequency = 1f;
        public float Speed = 0.25f;
        public float FadeDelay = 0.12f;
        public SpriteRenderer SpriteRenderer { get; private set; }
        private static GameObject _template;

        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(PulseSpriteEffectCoroutine(), Segment.FixedUpdate);
        }

        private IEnumerator<float> PulseSpriteEffectCoroutine()
        {
            while (isActiveAndEnabled)
            {
                var spriteGameObject = PrefabPool.Get(GetTemplate());
                var spriteRenderer = spriteGameObject.GetComponent<SpriteRenderer>();

                spriteGameObject.transform.position = transform.position;
                spriteGameObject.transform.rotation = transform.rotation;
                spriteGameObject.transform.localScale = transform.localScale;
                spriteGameObject.hideFlags = HideFlags.HideInHierarchy;

                spriteRenderer.drawMode = SpriteRenderer.drawMode;
                spriteRenderer.size = SpriteRenderer.size;
                spriteRenderer.sprite = SpriteRenderer.sprite;
                spriteRenderer.color = SpriteRenderer.color;
                spriteRenderer.sortingOrder = SpriteRenderer.sortingOrder;

                Timing.RunCoroutine(HoverAndFadeCoroutine(spriteRenderer), Segment.FixedUpdate);

                yield return Timing.WaitForSeconds(Frequency);
            }

            IEnumerator<float> HoverAndFadeCoroutine(SpriteRenderer spriteRenderer)
            {
                while (spriteRenderer.color.a > 0f)
                {
                    spriteRenderer.transform.position += Vector3.up * (Speed * 0.01f);
                    spriteRenderer.color = Vector4.MoveTowards(spriteRenderer.color, Color.clear, FadeDelay * 0.01f);

                    yield return Timing.WaitForOneFrame;
                }

                PrefabPool.Pool(spriteRenderer.gameObject);
            }

            GameObject GetTemplate()
            {
                if (_template == null)
                {
                    _template = new GameObject(nameof(PulseSpriteEffect), typeof(SpriteRenderer));
                    _template.hideFlags = HideFlags.HideInHierarchy;
                }

                return _template;
            }
        }
    }
}
