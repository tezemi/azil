﻿using System;
using UnityEngine;
using MyBox;

namespace Azil.Utilities
{
    [Serializable]
    public class SpriteAnimation
    {
        public float Delay = 0.15f;
        public Sprite[] Sprites;
        public bool IsValid => !Sprites.IsNullOrEmpty();
    }
}
