﻿using System.Collections.Generic;
using UnityEngine;
using MEC;

namespace Azil.Utilities
{
    public static class SoundEffect
    {
        private static GameObject _template;

        public static AudioSource Play(AudioClip sound)
        {
            var instance = PrefabPool.Get(GetTemplate());

            var audioSource = instance.GetComponent<AudioSource>();
            audioSource.clip = sound;

            audioSource.Play();

            Object.DontDestroyOnLoad(instance);
            PoolWhenNotPlaying(audioSource);

            return audioSource;
        }

        private static GameObject GetTemplate()
        {
            if (_template != null)
            {
                return _template;
            }

            var gameObject = new GameObject("Sound Effect", typeof(AudioSource));

            Object.DontDestroyOnLoad(gameObject);
            gameObject.hideFlags = HideFlags.HideInHierarchy;

            return gameObject;
        }

        private static void PoolWhenNotPlaying(AudioSource audioSource)
        {
            Timing.RunCoroutine(PoolWhenNotPlayingCoroutine(), Segment.FixedUpdate);

            IEnumerator<float> PoolWhenNotPlayingCoroutine()
            {
                yield return Timing.WaitUntilDone(new WaitUntil(() => !audioSource.isPlaying));

                PrefabPool.Pool(audioSource.gameObject);
            }
        }
    }
}
