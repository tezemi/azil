﻿using System.Collections.Generic;
using UnityEngine;
using MEC;

namespace Azil.Utilities
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteAnimator : MonoBehaviour
    {
        public bool PlayOnStart = true;        
        public SpriteAnimation Animation;
        public bool Playing { get; private set; }
        public SpriteRenderer SpriteRenderer { get; private set; }
        private CoroutineHandle _animateCoroutineHandle;

        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected virtual void OnEnable()
        {
            if (PlayOnStart)
            {
                Play();
            }
        }

        protected virtual void OnValidate()
        {
            if (Application.isPlaying || Animation == null || !Animation.IsValid)
                return;

            SpriteRenderer = GetComponent<SpriteRenderer>();
            SpriteRenderer.sprite = Animation.Sprites[0];
        }

        protected IEnumerator<float> AnimateCoroutine(bool playOnce)
        {
            var index = 0;

            Playing = true;
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitForSeconds(Animation.Delay);

                SpriteRenderer.sprite = Animation.Sprites[index++];

                if (index >= Animation.Sprites.Length)
                {
                    if (playOnce)
                    {
                        Stop();

                        yield break;
                    }
                    else
                    {
                        index = 0;
                    }
                }                    
            }            
        }

        public void Play(bool playOnce = false)
        {
            Stop();

            if (Animation == null || Animation.Sprites.Length == 0)
            {
                Debug.LogWarning("Tried to play a null animation.", this);

                return;
            }
            else
            {
                SpriteRenderer.sprite = Animation.Sprites[0];
            }

            _animateCoroutineHandle = Timing.RunCoroutine(AnimateCoroutine(playOnce), Segment.FixedUpdate);
        }

        public void Play(SpriteAnimation animation)
        {
            Animation = animation;

            Play(false);
        }

        public WaitUntil PlayOnce(SpriteAnimation animation)
        {
            Animation = animation;

            Play(true);

            return new WaitUntil(() => !Playing);
        }

        public void Stop()
        {
            Timing.KillCoroutines(_animateCoroutineHandle);

            Playing = false;
        }

        public bool IsPlaying(SpriteAnimation animation)
        {
            return Animation == animation;
        }
    }
}
