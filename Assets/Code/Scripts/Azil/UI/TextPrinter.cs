﻿using System.Collections.Generic;
using UnityEngine;
using MEC;
using TMPro;

namespace Azil.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class TextPrinter : MonoBehaviour
    {
        public int CharactersPerLine = 32;
        public int MaxLines = 3;
        public float Delay = 0.02f;
        public TMP_Text Text { get; private set; }
        private CoroutineHandle _printCoroutineHandle;
        private const float PUNCTUATION_DELAY_MULTIPLIER = 7f;

        protected virtual void Awake()
        {
            Text = GetComponent<TMP_Text>();
        }

        public virtual CoroutineHandle Print(string message)
        {
            Timing.KillCoroutines(_printCoroutineHandle);
            _printCoroutineHandle = Timing.RunCoroutine(PrintCoroutine(), Segment.FixedUpdate);

            return _printCoroutineHandle;

            IEnumerator<float> PrintCoroutine()
            {
                var outputText = message;
                for (var c = 0; c < outputText.Length; c++)
                {
                    if (c % CharactersPerLine == 0)
                    {
                        for (var b = c; b > 0; b--)
                        {
                            if (outputText[b] == ' ')
                            {
                                outputText = outputText.Insert(b, "\n");
                                outputText = outputText.Remove(b + 1, 1);

                                break;
                            }
                        }
                    }
                }

                Text.text = string.Empty;
                for (int i = 0; i < outputText.Length; i++) 
                {
                    Text.text += outputText[i].ToString();

                    yield return Timing.WaitForSeconds(Delay * (char.IsPunctuation(outputText[i]) && outputText[i] != '\'' ? PUNCTUATION_DELAY_MULTIPLIER : 1f));
                }
            }
        }
    }
}
