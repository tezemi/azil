﻿using Azil.Utilities;
using MEC;
using UnityEngine;

namespace Azil.UI.Transitions
{
    public class BattleTransitionCanvas : MonoBehaviour
    {
        public CoroutineHandle Transition(BattleTransition battleTransition)
        {
            var instance = PrefabPool.Get(battleTransition.gameObject);
            instance.transform.SetParent(transform, false);

            return Timing.RunCoroutine(instance.GetComponent<BattleTransition>().TransitionCoroutine(), Segment.FixedUpdate);
        }
    }
}
