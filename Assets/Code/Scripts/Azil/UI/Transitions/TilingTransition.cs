﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MEC;

namespace Azil.UI.Transitions
{
    [RequireComponent(typeof(RectTransform))]
    public class TilingTransition : BattleTransition
    {
        public List<Transform> Row1;
        public List<Transform> Row2;
        public List<Transform> Row3;
        public List<Transform> Row4;
        public List<Transform> Row5;
        public List<Transform> Row6;
        public List<Transform> Row7;
        public List<Transform> Row8;
        public List<Transform> Row9;
        private const float TIME_BETWEEN_FLIPS = 0.4f;
        private const float ROTATION_DESTINATION = 180f;
        private const float ROTATION_SPEED = 5f;

        public override IEnumerator<float> TransitionCoroutine()
        {
            yield return Timing.WaitForOneFrame;

            StartRotatingEven(Row1);

            yield return Timing.WaitForSeconds(TIME_BETWEEN_FLIPS);

            StartRotatingOdd(Row2);

            yield return Timing.WaitForSeconds(TIME_BETWEEN_FLIPS);

            StartRotatingOdd(Row1);
            StartRotatingEven(Row3);

            yield return Timing.WaitForSeconds(TIME_BETWEEN_FLIPS);

            StartRotatingEven(Row2);
            StartRotatingOdd(Row4);

            yield return Timing.WaitForSeconds(TIME_BETWEEN_FLIPS);

            StartRotatingOdd(Row3);
            StartRotatingEven(Row5);

            yield return Timing.WaitForSeconds(TIME_BETWEEN_FLIPS);

            StartRotatingEven(Row4);
            StartRotatingOdd(Row6);

            yield return Timing.WaitForSeconds(TIME_BETWEEN_FLIPS);

            StartRotatingOdd(Row5);
            StartRotatingEven(Row7);

            yield return Timing.WaitForSeconds(TIME_BETWEEN_FLIPS);

            StartRotatingEven(Row6);
            StartRotatingOdd(Row8);

            yield return Timing.WaitForSeconds(TIME_BETWEEN_FLIPS);

            StartRotatingOdd(Row7);
            StartRotatingEven(Row9);

            yield return Timing.WaitForSeconds(TIME_BETWEEN_FLIPS);

            StartRotatingEven(Row8);

            yield return Timing.WaitForSeconds(TIME_BETWEEN_FLIPS);

            StartRotatingOdd(Row9);

            yield return Timing.WaitForSeconds(TIME_BETWEEN_FLIPS * 4f);
        }

        private void StartRotatingEven(List<Transform> row)
        {
            Timing.RunCoroutine(StartRotating(row[0]), Segment.FixedUpdate);
            Timing.RunCoroutine(StartRotating(row[2]), Segment.FixedUpdate);
            Timing.RunCoroutine(StartRotating(row[4]), Segment.FixedUpdate);
        }

        private void StartRotatingOdd(List<Transform> row)
        {
            Timing.RunCoroutine(StartRotating(row[1]), Segment.FixedUpdate);
            Timing.RunCoroutine(StartRotating(row[3]), Segment.FixedUpdate);
        }

        private IEnumerator<float> StartRotating(Transform t)
        {
            var originalPosition = t.GetChild(0).position;

            while (t.rotation.eulerAngles.y != ROTATION_DESTINATION)
            {
                t.rotation = Quaternion.Euler(Vector3.MoveTowards(t.rotation.eulerAngles, new Vector3(0f, ROTATION_DESTINATION, 0f), ROTATION_SPEED));
                t.GetChild(0).position = originalPosition;
                t.GetChild(0).rotation = Quaternion.identity;
                t.GetChild(0).GetComponent<RawImage>().enabled = Mathf.Abs(t.rotation.eulerAngles.y) > 90f;

                yield return Timing.WaitForOneFrame;
            }
        }
    }
}
