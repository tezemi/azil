﻿using System.Collections.Generic;
using UnityEngine;
using MEC;
using Azil.Utilities;

namespace Azil.UI.Transitions
{
    public class SplitTransition : BattleTransition
    {
        public RectTransform TopMask;
        public RectTransform BottomMask;
        public AudioClip SplitSound;

        public override IEnumerator<float> TransitionCoroutine()
        {
            var topPositionFull = new Vector2(400f, 255f);
            var bottomPositionFull = new Vector2(-400f, -255f);

            var topInitialPosition = new Vector2(30f, 15f);
            var bottomInitialPosition = new Vector2(-30f, -15f);

            SoundEffect.Play(SplitSound);

            while (Vector2.Distance(TopMask.anchoredPosition, topInitialPosition) >= 3f ||
                   Vector2.Distance(BottomMask.anchoredPosition, bottomInitialPosition) >= 3f)
            {
                TopMask.localPosition = Vector2.Lerp(TopMask.localPosition, topInitialPosition, 0.06f);
                BottomMask.localPosition = Vector2.Lerp(BottomMask.localPosition, bottomInitialPosition, 0.06f);

                yield return Timing.WaitForOneFrame;
            }

            while (TopMask.anchoredPosition != topPositionFull || BottomMask.anchoredPosition != bottomPositionFull)
            {
                TopMask.localPosition = Vector2.MoveTowards(TopMask.localPosition, topPositionFull, 25f);
                BottomMask.localPosition = Vector2.MoveTowards(BottomMask.localPosition, bottomPositionFull, 25f);

                yield return Timing.WaitForOneFrame;
            }
        }
    }
}
