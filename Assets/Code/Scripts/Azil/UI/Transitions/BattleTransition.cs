﻿using System.Collections.Generic;
using UnityEngine;

namespace Azil.UI.Transitions
{    
    public abstract class BattleTransition : MonoBehaviour
    {
        public abstract IEnumerator<float> TransitionCoroutine();
    }
}
