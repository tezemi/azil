﻿using System.Collections.Generic;
using UnityEngine;
using MEC;

namespace Azil.UI.Transitions
{
    [RequireComponent(typeof(RectTransform))]
    public class TestBattleTransition : BattleTransition
    {
        public RectTransform RectTransform { get; private set; }
        private readonly Vector2 _destinationSize = new Vector2(1500f, 1000f);
        private const float START_SPEED = 1f;
        private const float SPEED_PER_FRAME = 0.025f;

        protected virtual void Awake()
        {
            RectTransform = GetComponent<RectTransform>();
        }

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(TransitionCoroutine());
        }

        public override IEnumerator<float> TransitionCoroutine()
        {
            var speed = START_SPEED;
            while (RectTransform.sizeDelta != _destinationSize)
            {
                RectTransform.sizeDelta = Vector2.MoveTowards(RectTransform.sizeDelta, _destinationSize, speed);

                speed += SPEED_PER_FRAME;

                yield return Timing.WaitForOneFrame;
            }
        }
    }
}
