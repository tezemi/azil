﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Azil.Utilities;
using System;

namespace Azil.UI
{
    [Serializable]
    public class MenuSounds
    {
        public AudioClip OnChangeSelectionSound;
        public AudioClip OnSubmitSound;

        public void Apply(params Button[] buttons)
        {
            foreach (var button in buttons)
            {
                if (!button.TryGetComponent<EventTrigger>(out var eventTrigger))
                {
                    eventTrigger = button.gameObject.AddComponent<EventTrigger>();
                }

                // On Selected
                var onSelectedEntry = new EventTrigger.Entry();
                onSelectedEntry.eventID = EventTriggerType.Select;
                onSelectedEntry.callback.AddListener((e) =>
                {
                    SoundEffect.Play(OnChangeSelectionSound);
                });

                eventTrigger.triggers.Add(onSelectedEntry);

                // On Pointer Enter
                var onPointerEnterEntry = new EventTrigger.Entry();
                onPointerEnterEntry.eventID = EventTriggerType.PointerEnter;
                onPointerEnterEntry.callback.AddListener((e) =>
                {
                    if (button.interactable)
                        SoundEffect.Play(OnChangeSelectionSound);
                });
                
                eventTrigger.triggers.Add(onPointerEnterEntry);

                // On Submit
                var onSubmitEntry = new EventTrigger.Entry();
                onSubmitEntry.eventID = EventTriggerType.Submit;
                onSubmitEntry.callback.AddListener((e) =>
                {
                    if (button.interactable)
                        SoundEffect.Play(OnSubmitSound);
                });

                eventTrigger.triggers.Add(onSubmitEntry);

                // On Click
                var pointerDownEntry = new EventTrigger.Entry();
                pointerDownEntry.eventID = EventTriggerType.PointerDown;
                pointerDownEntry.callback.AddListener((e) =>
                {
                    if (button.interactable)
                        SoundEffect.Play(OnSubmitSound);
                });

                eventTrigger.triggers.Add(pointerDownEntry);
            }
        }
    }
}
