﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MEC;

namespace Azil.UI.Battles
{
    public class BattleOptionsLayoutGroup : MonoBehaviour
    {
        public Button AttackButton;
        public Button MoveButton;
        public Button InventoryButton;
        public Button AssistButton;
        public Button FleeButton;
        public MenuSounds MenuSounds;
        private int _selectedOptionIndex;
        private BattleOption? _lastSelectedOption;

        public int SelectedOptionIndex
        {
            get
            {
                _lastSelectedOption = null;

                return _selectedOptionIndex;
            }
        }

        protected virtual void Awake()
        {
            MenuSounds.Apply(GetComponentsInChildren<Button>());
        }

        public void Show(bool showAttack, bool showMove, bool showInventory, bool showAssist, bool showFlee)
        {
            AttackButton.gameObject.SetActive(showAttack);
            MoveButton.gameObject.SetActive(showMove);
            InventoryButton.gameObject.SetActive(showInventory);
            AssistButton.gameObject.SetActive(showAssist);
            FleeButton.gameObject.SetActive(showFlee);

            gameObject.SetActive(true);

            GetComponentInChildren<Button>().Select();
            GetComponentInChildren<Button>().GetComponentInChildren<Text>().gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void OnAttackSelected()
        {
            _lastSelectedOption = BattleOption.Attack;
        }

        public void OnMoveSelected()
        {
            _lastSelectedOption = BattleOption.Move;
        }

        public void OnAssistSelect()
        {
            _lastSelectedOption = BattleOption.Assist;
        }

        public CoroutineHandle GetSelectedOption()
        {
            return Timing.RunCoroutine(GetSelectedOptionCoroutine());

            IEnumerator<float> GetSelectedOptionCoroutine()
            {
                yield return Timing.WaitUntilDone(new WaitUntil(() => _lastSelectedOption.HasValue));

                _selectedOptionIndex = (int)_lastSelectedOption.Value;
            }
        }

        private enum BattleOption
        {
            Attack,
            Move,
            Inventory,
            Assist,
            Flee
        }
    }
}
