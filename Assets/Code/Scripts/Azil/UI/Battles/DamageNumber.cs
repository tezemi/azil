﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Azil.Utilities;
using MEC;

namespace Azil.UI.Battles
{
    [RequireComponent(typeof(Text))]
    public class DamageNumber : MonoBehaviour
    {
        public Text Text { get; private set; }
        private float _intensity = INTENSITY_MIN;
        private const float VERTICAL_SPEED = 0.0125f;
        private const float HORIZONTAL_SPEED = 0.0125f;
        private const float INTENSITY_MIN = 2f;
        private const float INTENSITY_MAX = 3f;
        private const float FADE_OUT_SPEED = 0.01f;

        protected virtual void Awake()
        {
            Text = GetComponent<Text>();
        }

        protected virtual void FixedUpdate()
        {
            transform.position += Vector3.up * VERTICAL_SPEED;
            transform.position = new Vector3
            (
                transform.position.x + (Mathf.Sin(Time.time * _intensity) * HORIZONTAL_SPEED), 
                transform.position.y, 
                transform.position.z
            );
        }

        public void Show(int damage)
        {
            Timing.RunCoroutine(ShowCoroutine());

            IEnumerator<float> ShowCoroutine()
            {
                _intensity = Random.Range(INTENSITY_MIN, INTENSITY_MAX);
                Text.text = "-" + damage.ToString();

                var c = Text.color;
                c.a = 1f;
                Text.color = c;

                yield return Timing.WaitForSeconds(2f);

                while (Text.color.a > 0f)
                {
                    c = Text.color;
                    c.a -= FADE_OUT_SPEED;
                    Text.color = c;

                    yield return Timing.WaitForOneFrame;
                }

                PrefabPool.Pool(gameObject);
            }
        }
    }
}
