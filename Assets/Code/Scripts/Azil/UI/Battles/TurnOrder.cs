﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Azil.Utilities;
using MEC;


namespace Azil.UI.Battles
{
    public class TurnOrder : MonoBehaviour
    {
        public RectTransform IndicatorImage;
        public LayoutGroup LayoutGroup;
        public GameObject BattlerIconImagePrefab;
        public RectTransform RectTransform { get; private set; }
        private CoroutineHandle _moveIndicatorCoroutineHandle;
        private List<Image> _iconImages = new List<Image>();

        protected virtual void Awake()
        {
            RectTransform = GetComponent<RectTransform>();
        }

        public void Initialize(params Sprite[] icons)
        {
            foreach (var icon in icons)
            {
                var iconGameObject = PrefabPool.Get(BattlerIconImagePrefab);
                iconGameObject.transform.SetParent(LayoutGroup.transform, false);

                var iconImage = iconGameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>();
                iconImage.sprite = icon;

                _iconImages.Add(iconImage);
            }

            Timing.RunCoroutine(SetIndicatorPositionCoroutine());

            IEnumerator<float> SetIndicatorPositionCoroutine()
            {
                yield return Timing.WaitForOneFrame;
                yield return Timing.WaitForOneFrame;

                var first = _iconImages[0];
                IndicatorImage.transform.position = GetIndicatorPosition(first);
            }
        }

        public void MoveIndicator(int index)
        {
            Timing.KillCoroutines(_moveIndicatorCoroutineHandle);
            _moveIndicatorCoroutineHandle = Timing.RunCoroutine(MoveIndicatorCoroutine(), Segment.FixedUpdate);

            IEnumerator<float> MoveIndicatorCoroutine()
            {
                var icon = _iconImages[index];
                //var position = icon.transform.position + (Vector3.down * icon.GetComponent<RectTransform>().rect.height / 2f) + 
                //                                         (Vector3.left * icon.GetComponent<RectTransform>().rect.width / 2f);

                var position = GetIndicatorPosition(icon);                                                        

                while (Vector3.Distance(IndicatorImage.transform.position, position) > 1f)
                {
                    IndicatorImage.transform.position = Vector3.Lerp(IndicatorImage.transform.position, position, 0.08f);

                    yield return Timing.WaitForOneFrame;
                }
            }
        }

        public void UpdateOrder(params Sprite[] icons)
        {
            for (var i = 0; i < _iconImages.Count; i++)
            {
                _iconImages[i].sprite = icons[i];
            }
        }

        private Vector3 GetIndicatorPosition(Image icon)
        {
            return icon.transform.position + (Vector3.down * icon.rectTransform.rect.height / 2f);
        }
    }
}
