﻿using UnityEngine;

namespace Azil.UI.Battles
{
    public class BattleSelector : MonoBehaviour
    {
        public GameObject Target { get; set; }
        private const float HOVER_SPEED_MULTIPLIER = 0.75f;
        private const float HOVER_INTENSITY = 0.25f;
        private const float DISTANCE_FROM_TARGET = 0.75f;
        private const float SPEED_TO_TARGET = 0.12f;

        protected virtual void FixedUpdate()
        {
            if (Target != null)
            {
                transform.position = Vector3.Lerp
                (
                    transform.position, 
                    Target.transform.position + Vector3.up * ((Mathf.Sin(Time.fixedTime * HOVER_SPEED_MULTIPLIER) * HOVER_INTENSITY) + DISTANCE_FROM_TARGET),
                    SPEED_TO_TARGET
                );
            }                
        }

        public void Show(Vector3 initialPosition)
        {
            transform.position = initialPosition;
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
