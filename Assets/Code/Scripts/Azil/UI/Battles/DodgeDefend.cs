﻿using UnityEngine;

namespace Azil.UI.Battles
{
    public class DodgeDefend : MonoBehaviour
    {
        public bool DodgeTriggered { get; private set; }
        public bool DefendTriggered { get; private set; }

        public void Show()
        {
            gameObject.SetActive(true);

            DodgeTriggered = false;
            DefendTriggered = false;
        }

        public void Hide()
        {
            gameObject.SetActive(false);

            DodgeTriggered = false;
            DefendTriggered = false;
        }

        public void OnDodge()
        {
            DodgeTriggered = true;
        }

        public void OnDefend()
        {
            DefendTriggered = true;
        }
    }
}
