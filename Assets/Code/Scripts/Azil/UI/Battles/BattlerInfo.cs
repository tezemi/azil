﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Utilities;
using MEC;

namespace Azil.UI.Battles
{
    public abstract class BattlerInfo : MonoBehaviour
    {
        public GameObject Parent { get; set; }
        protected abstract float Offset { get; }
        private const float LERP_SPEED = 0.24f;
        private const float FADE_TIME = 0.25f;

        protected virtual void FixedUpdate()
        {
            transform.position = Vector3.Lerp(transform.position, Parent.transform.position + Vector3.up * Offset, LERP_SPEED);
        }

        public CoroutineHandle FadeOut()
        {            
            return Timing.RunCoroutine(FadeOutCoroutine(), Segment.FixedUpdate);

            IEnumerator<float> FadeOutCoroutine()
            {
                var graphics = GetComponentsInChildren<Graphic>();
                if (!graphics.Any())
                    yield break;

                foreach (var graphic in graphics)
                {
                    if (!graphic.enabled)
                        continue;

                    graphic.FadeOut(FADE_TIME);
                }

                yield return Timing.WaitUntilDone(new WaitUntil(() => graphics[0].color.a == 0f));
            }
        }

        public CoroutineHandle FadeIn(bool clearFirst = false)
        {
            return Timing.RunCoroutine(FadeInCoroutine(), Segment.FixedUpdate);

            IEnumerator<float> FadeInCoroutine()
            {
                var graphics = GetComponentsInChildren<Graphic>(true);
                if (!graphics.Any())
                    yield break;

                if (clearFirst)
                {
                    foreach (var graphic in graphics)
                    {
                        graphic.Clear();
                    }
                }
                
                foreach (var graphic in graphics)
                {
                    if (!graphic.enabled)
                        continue;

                    graphic.FadeIn(FADE_TIME);
                }

                yield return Timing.WaitUntilDone(new WaitUntil(() => graphics[0].color.a == 0f));
            }
        }

        public void Hide()
        {
            var graphics = GetComponentsInChildren<Graphic>();
            foreach (var graphic in graphics)
            {
                graphic.Clear();
            }
        }
    }
}
