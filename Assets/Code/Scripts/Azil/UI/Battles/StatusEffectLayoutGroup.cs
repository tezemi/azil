﻿using System.Collections.Generic;
using Azil.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Azil.UI.Battles
{
    public class StatusEffectLayoutGroup : BattlerInfo
    {
        public GameObject StatusEffectIconImagePrefab;
        protected override float Offset => -1f;
        private readonly List<Image> _icons = new List<Image>();

        public void AddStatusEffect(Sprite sprite, int duration)
        {
            var instance = PrefabPool.Get(StatusEffectIconImagePrefab);

            var text = instance.GetComponentInChildren<Text>();

            instance.GetComponent<Image>().sprite = sprite;
            text.text = duration.ToString();

            if (duration <= 0)
                text.enabled = false;
            else
                text.enabled = true;                

            instance.transform.SetParent(transform, false);

            _icons.Add(instance.GetComponent<Image>());

            if (_icons.Count == 1)
            {
                FadeIn(true);
            }
        }

        public void RemoveStatusEffect(Sprite sprite)
        {
            for (var i = 0; i < _icons.Count; i++)
            {
                var image = _icons[i];
                if (image.sprite == sprite)
                {
                    _icons.Remove(image);
                    PrefabPool.Pool(image.transform.parent.gameObject);
                }
            }
        }

        public void UpdateStatusEffectDuration(Sprite sprite, int duration)
        {
            foreach (var image in _icons)
            {
                if (image.sprite == sprite)
                {
                    image.GetComponentInChildren<Text>().text = duration.ToString();
                }
            }
        }
    }
}
