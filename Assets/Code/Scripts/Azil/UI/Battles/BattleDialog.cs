﻿using UnityEngine;
using MEC;

namespace Azil.UI.Battles
{
    public class BattleDialog : MonoBehaviour
    {
        public TextPrinter BattleDialogTextPrinter;
        public DialogTail DialogTail;
        private bool _pushed;
        private const float SCROLL_AMOUNT = 12f;

        public CoroutineHandle ShowDialog(string dialog)
        {
            if (_pushed)
            {
                BattleDialogTextPrinter.Text.rectTransform.anchoredPosition -= Vector2.up * SCROLL_AMOUNT;

                _pushed = false;
            }            

            return BattleDialogTextPrinter.Print(dialog);
        }

        protected virtual void FixedUpdate()
        {
            if (BattleDialogTextPrinter.Text.textInfo.lineCount >= 3 && !_pushed)
            {
                BattleDialogTextPrinter.Text.rectTransform.anchoredPosition += Vector2.up * SCROLL_AMOUNT;

                _pushed = true;
            }
        }
    }
}
