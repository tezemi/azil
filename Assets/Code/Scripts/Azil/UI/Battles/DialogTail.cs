﻿using UnityEngine;

namespace Azil.UI.Battles
{
    [RequireComponent(typeof(RectTransform))]
    public class DialogTail : MonoBehaviour
    {
        public Camera Camera { get; set; }
        public Vector3 WorldPosition { get; set; }
        public RectTransform RectTransform { get; private set; }
        public Vector3 CanvasPosition => RectTransformUtility.WorldToScreenPoint(Camera, WorldPosition);
        private Vector3 _initialPosition;

        protected virtual void Awake()
        {
            RectTransform = GetComponent<RectTransform>();

            _initialPosition = RectTransform.position;

            gameObject.SetActive(false);
        }

        public void Show(Camera camera, Vector3 worldPosition)
        {
            Camera = camera;
            WorldPosition = worldPosition;

            RectTransform.position = _initialPosition;

            SetRotation();
            SetSize();
            SetPosition();

            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void SetRotation()
        {
            var direction = CanvasPosition - _initialPosition;
            RectTransform.up = direction;
        }

        public void SetSize()
        {
            var distance = Vector2.Distance(_initialPosition, CanvasPosition);
            var size = distance - 100f;
            RectTransform.sizeDelta = new Vector2(RectTransform.sizeDelta.x, size);
        }

        public void SetPosition()
        {
            var pos = (_initialPosition + CanvasPosition) / 2f;
            RectTransform.position = pos - RectTransform.up * 100f;
        }
    }
}
