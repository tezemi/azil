﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Azil.Utilities;
using MEC;
using TMPro;

namespace Azil.UI.Battles
{
    public class AttackSelection : MonoBehaviour
    {        
        public TMP_Text DescriptionText;
        public TMP_Text DisabledReasonText;
        public TMP_Text SpecialText;
        public Transform ScrollViewContent;
        public GameObject AttackNameTextPrefab;
        private bool _cancelled;
        private int _selectedAttackIndex = -1;

        public void Show(params AttackMenuOption[] attackMenuOptions)
        {
            foreach (Transform child in ScrollViewContent)
            {
                Destroy(child.gameObject);
            }

            var i = 0;
            foreach (var attackMenuOption in attackMenuOptions)
            {
                var instanceGameObject = Instantiate(AttackNameTextPrefab, ScrollViewContent);
                var instanceText = instanceGameObject.GetComponent<TMP_Text>();
                var instanceEventTrigger = instanceGameObject.GetComponent<EventTrigger>();
                var instanceButton = instanceGameObject.GetComponent<Button>();

                instanceText.text = attackMenuOption.Name;

                var onSelectedEntry = new EventTrigger.Entry();
                onSelectedEntry.eventID = EventTriggerType.Select;
                onSelectedEntry.callback.AddListener(OnAttackButtonSelected);

                instanceEventTrigger.triggers.Add(onSelectedEntry);

                if (attackMenuOption.Interactable)
                    instanceButton.onClick.AddListener(() => { _selectedAttackIndex = instanceGameObject.transform.GetSiblingIndex(); });

                if (i == 0)
                    OnAttackButtonSelected(null);

                //instanceButton.interactable = attackMenuOption.Interactable;
                var colorBlock = instanceButton.colors;
                var normalColor = colorBlock.normalColor;
                var selectedColor = colorBlock.selectedColor;

                normalColor.a *= attackMenuOption.Interactable ? 1f : 0.5f;
                selectedColor.a *= attackMenuOption.Interactable ? 1f : 0.5f;

                colorBlock.normalColor = normalColor;
                colorBlock.selectedColor = selectedColor;
                instanceButton.colors = colorBlock;

                if (i == 0)
                    instanceButton.Select();

                i++;

                void OnAttackButtonSelected(BaseEventData eventData)
                {
                    DescriptionText.text = attackMenuOption.Description;
                    SpecialText.text = attackMenuOption.Cost.ToString();
                    DisabledReasonText.text = attackMenuOption.DisabledReasonText;
                }
            }

            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Cancel()
        {
            _cancelled = true;
        }

        public CoroutineHandle GetAttackIndex(out Out<BoxedInt> attackIndex)
        {
            var boxedInt = new BoxedInt();

            attackIndex = new Out<BoxedInt>();            
            attackIndex.Result = boxedInt;

            return Timing.RunCoroutine(GetAttackCoroutine());

            IEnumerator<float> GetAttackCoroutine()
            {
                var input = new AzilInputActions();
                input.UI.Cancel.Enable();

                yield return Timing.WaitForOneFrame;
                yield return Timing.WaitUntilDone(new WaitUntil(() => _selectedAttackIndex != -1 || _cancelled || input.UI.Cancel.triggered));

                boxedInt.Value = _selectedAttackIndex;

                yield return Timing.WaitForOneFrame;

                _selectedAttackIndex = -1;
                _cancelled = false;
                input.UI.Cancel.Disable();
            }
        }

        public struct AttackMenuOption
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public string DisabledReasonText { get; set; }
            public int Cost { get; set; }
            public bool Interactable { get; set; }

            public AttackMenuOption(string name, string description, int cost, bool interactable)
            {
                Name = name;
                Description = description;
                DisabledReasonText = string.Empty;
                Cost = cost;
                Interactable = interactable;
            }

            public AttackMenuOption(string name, string description, string disabledReasonText, int cost, bool interactable)
            {
                Name = name;
                Description = description;
                DisabledReasonText = disabledReasonText;
                Cost = cost;
                Interactable = interactable;
            }
        }
    }
}
