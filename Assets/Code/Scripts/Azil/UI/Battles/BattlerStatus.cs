﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Utilities;
using MEC;

namespace Azil.UI.Battles
{
    [RequireComponent(typeof(RectTransform))]
    public class BattlerStatus : BattlerInfo
    {
        public Image HealthImageIcon;
        public Image SpecialImageIcon;
        public Text HealthText;
        public Text SpecialText;
        protected override float Offset => 1f;

        public void SetStatus(int health, int special)
        {
            HealthText.text = health.ToString();
            SpecialText.text = special.ToString();
        }
    }
}
