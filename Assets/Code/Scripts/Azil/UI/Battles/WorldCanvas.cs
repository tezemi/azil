﻿using Azil.Utilities;
using UnityEngine;

namespace Azil.UI.Battles
{
    public class WorldCanvas : MonoBehaviour
    {
        public BattleOptionsLayoutGroup BattleOptionsLayoutGroup;
        public BattleSelector BattleSelector;
        public GameObject DeathTimerPrefab;
        public GameObject DamageNumberPrefab;
        public GameObject BattlerStatusPrefab;
        public GameObject StatusEffectsLayoutGroupPrefab;

        public DeathTimer CreateDeathTimer(GameObject parent, Vector3 position)
        {
            var deathTimerGameObject = PrefabPool.Get(DeathTimerPrefab);
            deathTimerGameObject.transform.SetParent(transform);
            deathTimerGameObject.transform.position = position;

            var deathTimer = deathTimerGameObject.GetComponent<DeathTimer>();
            deathTimer.Parent = parent;

            return deathTimer;
        }

        public DamageNumber CreateDamageNumber(Vector3 position, int damage)
        {
            var damageNumberGameObject = PrefabPool.Get(DamageNumberPrefab);
            damageNumberGameObject.transform.SetParent(transform);
            damageNumberGameObject.transform.position = position;

            var damageNumber = damageNumberGameObject.GetComponent<DamageNumber>();
            damageNumber.Show(damage);

            return damageNumber;
        }

        public BattlerStatus CreateBattlerStatus(GameObject parent, int health, int special)
        {
            var battlerStatusGameObject = PrefabPool.Get(BattlerStatusPrefab);
            battlerStatusGameObject.transform.SetParent(transform);

            var battlerStatus = battlerStatusGameObject.GetComponent<BattlerStatus>();
            battlerStatus.Parent = parent;
            battlerStatus.SetStatus(health, special);

            return battlerStatus;
        }

        public StatusEffectLayoutGroup CreateStatusEffectsLayoutGroupPrefab(GameObject parent)
        {
            var statusEffectLayoutGroupGameObject = PrefabPool.Get(StatusEffectsLayoutGroupPrefab);
            statusEffectLayoutGroupGameObject.transform.SetParent(transform);

            var statusEffectLayoutGroup = statusEffectLayoutGroupGameObject.GetComponent<StatusEffectLayoutGroup>();
            statusEffectLayoutGroup.Parent = parent;

            return statusEffectLayoutGroup;
        }
    }
}
