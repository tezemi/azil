﻿using UnityEngine.UI;

namespace Azil.UI.Battles
{
    public class DeathTimer : BattlerInfo
    {
        public Text DeathTimerText;
        protected override float Offset => 1f;

        public void SetTime(int seconds)
        {
            DeathTimerText.text = seconds.ToString();
        }
    }
}
