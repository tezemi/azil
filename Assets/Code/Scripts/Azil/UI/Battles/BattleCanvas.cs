﻿using UnityEngine;
using UnityEngine.UI;

namespace Azil.UI.Battles
{
    [RequireComponent(typeof(Canvas))]
    public class BattleCanvas : MonoBehaviour
    {
        public TurnOrder TurnOrder;
        public AttackSelection AttackSelection;
        public BattleDialog BattleDialog;
        public DodgeDefend DodgeDefend;
        public Button CancelButton;
        public Canvas Canvas { get; private set; }

        protected virtual void Awake()
        {
            Canvas = GetComponent<Canvas>();
        }
    }
}
