﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Azil.Utilities;
using MEC;

namespace Azil.UI.Battles.Attacks
{
    public class StatusEffectMeterAttackInteraction : AttackInteraction<float>
    {
        public Image PointerImage;
        public Image MeterImage;
        public Image StatusEffectImage;
        private bool _movePointerImage;
        private const float BASE_SPEED = 10f;
        private const float MAX_SPEED_INCREASE_MODIFIER = 0.75f;
        private const float SPEED_FLUCTUATION_INTENSITY = 1f;
        
        public override IEnumerator<float> PerformInteraction(Out<float> returnValue)
        {
            var input = new AzilInputActions();
            input.UI.Submit.Enable();
            input.UI.Click.Enable();

            yield return Timing.WaitUntilDone(new WaitUntil(() => !input.UI.Click.triggered));
            yield return Timing.WaitForSeconds(0.25f);
                        
            var pointerMin = new Vector2
            (
                PointerImage.rectTransform.anchoredPosition.x, 
                MeterImage.rectTransform.anchoredPosition.y - MeterImage.rectTransform.sizeDelta.y / 2f
            );

            var pointerMax = new Vector2
            (
                PointerImage.rectTransform.anchoredPosition.x, 
                MeterImage.rectTransform.anchoredPosition.y + MeterImage.rectTransform.sizeDelta.y / 2f
            );

            var initialPointerPos = PointerImage.rectTransform.anchoredPosition;

            _movePointerImage = true;

            // This moves the pointer, which needs to be on the fixed update
            Timing.RunCoroutine(MovePointerImageCoroutine(pointerMin, pointerMax), Segment.FixedUpdate);

            // But this coroutine is on update for input purposes
            yield return Timing.WaitUntilDone(new WaitUntil(() => input.UI.Submit.triggered || input.UI.Click.triggered));

            _movePointerImage = false;

            var normalizedValue = MathUtilities.GetRangeClamped(PointerImage.rectTransform.anchoredPosition.y, pointerMin.y, pointerMax.y, 0f, 1f);

            input.UI.Submit.Disable();
            input.UI.Click.Disable();

            returnValue.Result = normalizedValue;

            var pointerRightPos = PointerImage.rectTransform.anchoredPosition + Vector2.right * 75f;
            while (PointerImage.rectTransform.anchoredPosition != pointerRightPos)
            {
                PointerImage.rectTransform.anchoredPosition = Vector2.MoveTowards(PointerImage.rectTransform.anchoredPosition, pointerRightPos, 5f);

                yield return Timing.WaitForOneFrame;
            }

            yield return Timing.WaitForSeconds(1f);

            PointerImage.rectTransform.anchoredPosition = initialPointerPos;
        }

        private IEnumerator<float> MovePointerImageCoroutine(Vector2 pointerMin, Vector2 pointerMax)
        {
            var movingToMin = true;
            var frameCount = 0;

            while (_movePointerImage)
            {
                PointerImage.rectTransform.anchoredPosition = Vector3.MoveTowards
                (
                    PointerImage.rectTransform.anchoredPosition,
                    movingToMin ? pointerMin : pointerMax,
                    (Mathf.Sin(frameCount++ * SPEED_FLUCTUATION_INTENSITY) * MAX_SPEED_INCREASE_MODIFIER) + BASE_SPEED
                );

                if (movingToMin && PointerImage.rectTransform.anchoredPosition == pointerMin)
                {
                    movingToMin = false;
                }
                else if (!movingToMin && PointerImage.rectTransform.anchoredPosition == pointerMax)
                {
                    movingToMin = true;
                }

                var val = MathUtilities.GetRangeClamped(PointerImage.rectTransform.anchoredPosition.y, pointerMin.y, pointerMax.y, 0f, 1f);
                StatusEffectImage.color = new Color
                (
                    StatusEffectImage.color.r, 
                    StatusEffectImage.color.b, 
                    StatusEffectImage.color.g,
                    (Mathf.Abs(val - 0.5f) < 0.1f) ? 1f : 0.5f
                );

                yield return Timing.WaitForOneFrame;
            }
        }
    }
}
