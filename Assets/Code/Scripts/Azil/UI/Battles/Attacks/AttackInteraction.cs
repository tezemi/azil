﻿using System.Collections.Generic;
using UnityEngine;
using Azil.Utilities;

namespace Azil.UI.Battles.Attacks
{
    public abstract class AttackInteraction<T> : MonoBehaviour
    {
        public abstract IEnumerator<float> PerformInteraction(Out<T> returnValue);
    }
}
