﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Azil.UI.Menus
{
    public abstract class GameMenu<T> : MonoBehaviour where T : UIBehaviour
    {
        public bool DisableMouseInput;
        public bool DisableKeyboardInput;
        public int Rows;
        public int Columns;
        public int DefaultRow;
        public int DefaultColumn;
        [HideInInspector]
        public T[] SerializedOptions;
        public T[,] Options { get; set; }
        public bool MouseInControl { get; set; }
        public bool DisableInput { get; set; }
        public (int, int) CurrentlySelectedOption { get; set; }
        public (int, int) LastSelectedOption { get; protected set; }
        public const float MouseMovementAmount = 1f;
        private Vector2 _previousMousePosition;

        public bool ShouldUseMouse
        {
            get
            {
                if (Vector2.Distance(Input.mousePosition, _previousMousePosition) > MouseMovementAmount)
                {
                    MouseInControl = true;
                    _previousMousePosition = Input.mousePosition;

                    return true;
                }

                return false;
            }
        }

        public bool ShouldUseKeyboard
        {
            get
            {
                if (Input.anyKeyDown)
                {
                    MouseInControl = false;
                    _previousMousePosition = Input.mousePosition;

                    return true;
                }

                return false;
            }
        }

        public T SelectedElement
        {
            get
            {
                if (Options.Length > 0)
                {
                    return Options[CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2];
                }

                return null;
            }
        }

        protected virtual void Awake()
        {
            UpdateOptions();
        }

        protected virtual void OnEnable()
        {
            CurrentlySelectedOption = (DefaultRow, DefaultColumn);
        }

        protected virtual void Update()
        {
            if (DisableInput) return;

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnBackOut();
                return;
            }

            //if ((MouseInControl || ShouldUseMouse) && !DisableMouseInput)
            //{
            //    MouseUpdate();
            //}

            if ((!MouseInControl || ShouldUseKeyboard) && !DisableKeyboardInput)
            {
                KeyboardUpdate();
            }

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                OnSelected(CurrentlySelectedOption);
            }
        }

        protected void KeyboardUpdate()
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) &&
            CurrentlySelectedOption.Item1 - 1 >= 0 &&
            Options[CurrentlySelectedOption.Item1 - 1, CurrentlySelectedOption.Item2] != null)
            {
                CurrentlySelectedOption = (CurrentlySelectedOption.Item1 - 1, CurrentlySelectedOption.Item2);
                LastSelectedOption = CurrentlySelectedOption;
            }

            if (Input.GetKeyDown(KeyCode.DownArrow) &&
            Options.GetLength(0) > CurrentlySelectedOption.Item1 + 1 &&
            Options[CurrentlySelectedOption.Item1 + 1, CurrentlySelectedOption.Item2] != null)
            {
                CurrentlySelectedOption = (CurrentlySelectedOption.Item1 + 1, CurrentlySelectedOption.Item2);
                LastSelectedOption = CurrentlySelectedOption;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow) &&
            CurrentlySelectedOption.Item2 - 1 >= 0 &&
            Options[CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2 - 1] != null)
            {
                CurrentlySelectedOption = (CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2 - 1);
                LastSelectedOption = CurrentlySelectedOption;
            }

            if (Input.GetKeyDown(KeyCode.RightArrow) &&
            Options.GetLength(1) > CurrentlySelectedOption.Item2 + 1 &&
            Options[CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2 + 1] != null)
            {
                CurrentlySelectedOption = (CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2 + 1);
                LastSelectedOption = CurrentlySelectedOption;
            }
        }

        protected virtual void UpdateOptions()
        {
            Options = new T[Rows, Columns];
            for (var x = 0; x < Options.GetLength(0); x++)
            {
                for (var y = 0; y < Options.GetLength(1); y++)
                {
                    Options[x, y] = SerializedOptions[x * Columns + y];
                }
            }
        }

        protected virtual void ClearOptions()
        {
            Rows = 0;
            Columns = 0;
            SerializedOptions = new T[0];
            UpdateOptions();
        }

        public virtual void AddOption(T option, bool addRow)
        {
            List<T> options = SerializedOptions.ToList();
            options.Add(option);
            SerializedOptions = options.ToArray();
            if (addRow)
            {
                if (Columns == 0)
                {
                    Columns++;
                }

                Rows++;
            }
            else
            {
                if (Rows == 0)
                {
                    Rows++;
                }

                Columns++;
            }

            UpdateOptions();
        }

        protected abstract void OnBackOut();

        protected abstract void OnSelected((int, int) itemClicked);
    }
}
