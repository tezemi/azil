﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Azil.UI.Menus
{
    public abstract class TextMenu : GameMenu<Text>
    {
        public Color SelectedColor = Color.red;
        public Color UnselectedColor = Color.blue;
        public List<Text> DisableColorsFor { get; protected set; } = new List<Text>();

        protected override void Update()
        {
            foreach (Text text in Options)
            {
                if (text == null || DisableColorsFor.Contains(text)) continue;

                text.color = UnselectedColor;
            }

            if (!DisableColorsFor.Contains(Options[CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2]))
            {
                Options[CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2].color = SelectedColor;
            }

            base.Update();
        }
    }
}
