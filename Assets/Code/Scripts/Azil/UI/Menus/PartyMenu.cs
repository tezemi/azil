﻿using System.Collections.Generic;
using Azil.Characters;
using UnityEngine;

namespace Azil.UI.Menus
{
    public class PartyMenu : TextIndentionMenu
    {
        public GameObject PauseMenu;
        public GameObject CharacterOptionPrefab;
        protected readonly List<GameObject> CharacterOptions = new List<GameObject>();

        protected override void OnEnable()
        {
            base.OnEnable();

            //foreach (Character character in SaveFile.LoadedSaveFile.Party)
            //{
            //    GameObject option = Instantiate(CharacterOptionPrefab);
            //    option.transform.SetParent(transform, false);
            //    option.GetComponent<CharacterOption>().Character = character;
            //    CharacterOptions.Add(option);
            //    AddOption(option.GetComponent<CharacterOption>().CharacterName, true);
            //}
        }

        protected virtual void OnDisable()
        {
            ClearOptions();
            foreach (GameObject obj in CharacterOptions)
            {
                Destroy(obj);
            }

            CharacterOptions.Clear();
        }

        protected override void OnBackOut()
        {
            gameObject.SetActive(false);
            PauseMenu.SetActive(true);
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            gameObject.SetActive(false);
            //SaveFile.LoadedSaveFile.Party[itemClicked.Item1].OpenExperienceWebMenu();
            //SaveFile.LoadedSaveFile.Party[itemClicked.Item1].ExperienceWebMenuInstance.GetComponent<ExperienceWebMenu>().PartyMenu = gameObject;
        }
    }
}
