﻿using UnityEngine;

namespace Azil.UI.Menus
{
    public class PauseMenu : TextIndentionMenu
    {
        public GameObject PartyMenu;
        public bool Open { get; set; }

        protected override void OnEnable()
        {
            base.OnEnable();
            Open = true;
        }

        protected override void OnBackOut()
        {
            Open = false;
            gameObject.SetActive(false);
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            switch (itemClicked.Item1)
            {
                case 0:
                    gameObject.SetActive(false);
                    PartyMenu.SetActive(true);
                    break;
                case 1:
                    Debug.LogWarning("Not available yet.");
                    break;
                case 2:
                    Debug.LogWarning("Not available yet.");
                    break;
                case 3:
                    OnBackOut();
                    break;
                case 4:
                    Debug.LogWarning("Not available yet.");
                    break;
                case 5:
                    Debug.LogWarning("Not available yet.");
                    break;
            }
        }
    }
}
