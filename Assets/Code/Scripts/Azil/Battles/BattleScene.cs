﻿using UnityEngine;
using Azil.Utilities;
using Azil.Characters;
using Azil.UI.Battles;
using MyBox;
using UnityEngine.SceneManagement;
using Azil.Battles.ScriptedSequences;

namespace Azil.Battles
{
    
    public class BattleScene : MonoBehaviour
    {
        [Header("Scene")]
        public Tile[] Tiles;
        public GameObject RangeSelectionVisual;
        public BattleCanvas BattleCanvas;
        public WorldCanvas WorldCanvas; 
        public BattleCamera BattleCamera;
        public BattleScriptedSequence BattleScriptedSequence;
        [Header("Prefabs")]
        public GameObject BattlerPrefab;
        [Header("Debug")]
        public CharacterBase[] DebugBattleCharacters;

        protected virtual void Update()
        {
            if (Input.GetKeyDown(KeyCode.B))
            {
                StartDebugBattle();
            }
        }

        [ButtonMethod]
        public void StartDebugBattle()
        {
            var battlers = new Battler[DebugBattleCharacters.Length];

            for (var i = 0; i < DebugBattleCharacters.Length; i++)
            {
                var character = new Character(DebugBattleCharacters[i]);
                var battlerGameObject = PrefabPool.Get(BattlerPrefab);
                var battler = battlerGameObject.GetComponent<Battler>();
                battler.Character = character;
                battlers[i] = battler;
            }

            var battle = new Battle(battlers, Tiles, RangeSelectionVisual, BattleCanvas, WorldCanvas, BattleCamera, BattleScriptedSequence);
            battle.StartBattle();
        }

        public Battle StartBattle(Character[] characters)
        {
            var battlers = new Battler[characters.Length];

            for (var i = 0; i < characters.Length; i++)
            {
                var character = characters[i];
                var battlerGameObject = PrefabPool.Get(BattlerPrefab);                
                var battler = battlerGameObject.GetComponent<Battler>();
                battler.Character = character;
                battlers[i] = battler;

                SceneManager.MoveGameObjectToScene(battlerGameObject, gameObject.scene);
            }

            var battle = new Battle(battlers, Tiles, RangeSelectionVisual, BattleCanvas, WorldCanvas, BattleCamera, BattleScriptedSequence);
            battle.StartBattle();

            return battle;
        }
    }
}
