﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Azil.Characters.AttackConfigs;
using MEC;
using MyBox;

namespace Azil.Battles.Attacks
{
    public class TestEnemyAttack : Attack
    {
        public override bool HasValidTargets(Battle battle, AttackConfig config)
        {
            return true;
        }

        public override IEnumerator<float> AttackCoroutine(Battle battle, AttackConfig config)
        {
            yield return Timing.WaitForSeconds(1f);

            var target = battle.Battlers.Where(b => b.Team != battle.ActiveBattler.Team).GetRandom();

            battle.StartDodgeDefend(target);
            battle.BattleCamera.Focus = target.gameObject;

            yield return Timing.WaitUntilDone(battle.ActiveBattler.MoveToPoint(target.transform.position + Vector3.right * 2f));                       

            target.Hurt(50);

            battle.StopDodgeDefend(target);

            yield return Timing.WaitForSeconds(1f);

            battle.ActiveBattler.MoveToTile(battle.ActiveBattler.OccupyingTile);

            yield return Timing.WaitForSeconds(1f);
        }
    }
}

