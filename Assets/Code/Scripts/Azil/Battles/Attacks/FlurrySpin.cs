﻿using System.Linq;
using System.Collections.Generic;
using Azil.Utilities;
using Azil.Characters.AttackConfigs;
using UnityEngine;
using MEC;

namespace Azil.Battles.Attacks
{
    public class FlurrySpin : Attack
    {
        public override bool HasValidTargets(Battle battle, AttackConfig config)
        {
            return true;
        }

        public override IEnumerator<float> AttackCoroutine(Battle battle, AttackConfig config)
        {
            var user = battle.ActiveBattler;

            battle.BattlerStatuses[user].FadeOut();

            battle.BattleCamera.Point = battle.Battlers.Where(b => b.Team != user.Team).Select(b => b.transform.position).Center();

            yield return Timing.WaitForSeconds(1f);
            
            user.gameObject.SetActive(false);

            var flurrySpinGameObject = PrefabPool.Get((config as FlurrySpinAttackConfig).FlurrySpinProjectilePrefab);
            var flurrySpinProjectile = flurrySpinGameObject.GetComponent<FlurrySpinProjectile>();

            flurrySpinProjectile.Initialize(user.transform.position, user);

            yield return Timing.WaitForSeconds(7f);

            flurrySpinProjectile.enabled = false;

            const float returnSpeed = 0.24f;
            while (Vector3.Distance(flurrySpinGameObject.transform.position, user.transform.position) > returnSpeed)
            {
                flurrySpinGameObject.transform.position = Vector3.MoveTowards(flurrySpinGameObject.transform.position, user.transform.position, returnSpeed);

                yield return Timing.WaitForOneFrame;
            }

            yield return Timing.WaitForSeconds(0.25f);

            PrefabPool.Pool(flurrySpinGameObject);

            battle.BattlerStatuses[user].FadeIn();

            user.gameObject.SetActive(true);

            battle.BattleCamera.Point = null;
        }
    }
}

