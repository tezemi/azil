﻿using System.Collections.Generic;
using Azil.Characters.AttackConfigs;
using MEC;

namespace Azil.Battles.Attacks
{
    public class TutorialEnemyAttack : Attack
    {
        public override bool HasValidTargets(Battle battle, AttackConfig config)
        {
            return true;
        }

        public override IEnumerator<float> AttackCoroutine(Battle battle, AttackConfig config)
        {
            var c = config as SimpleEnemyAttackConfig;

            yield return Timing.WaitForSeconds(1f);

            var target = battle.Battlers[0];

            battle.StartDodgeDefend(target);
            battle.BattleCamera.Focus = target.gameObject;

            battle.ActiveBattler.SpriteAnimator.Play(c.WindUpSpriteAnimation);

            yield return Timing.WaitUntilDone(battle.ActiveBattler.MoveNextTo(target, false));

            yield return Timing.WaitForSeconds(0.5f);

            battle.ActiveBattler.SpriteAnimator.Play(c.SwingSpriteAnimation);

            target.Hurt(battle.ActiveBattler.Character.GetDamageFromAttack(15));

            battle.StopDodgeDefend(target);

            yield return Timing.WaitForSeconds(0.5f);

            battle.ActiveBattler.SpriteAnimator.Play(battle.ActiveBattler.Character.CharacterBase.CharactersSprites.BattleIdle);
            yield return Timing.WaitUntilDone(battle.ActiveBattler.MoveBackToTile());

            yield return Timing.WaitForSeconds(1f);
        }
    }
}

