﻿using Azil.Utilities;
using UnityEngine;

namespace Azil.Battles.Attacks
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class FlurrySpinProjectile : MonoBehaviour
    {
        public int BaseDamage = 15;
        public float Speed = 15f;
        public Battler Owner { get; private set; }
        public Rigidbody2D Rigidbody { get; private set; }
        private AzilInputActions _inputActions;

        protected virtual void OnEnable()
        {
            Rigidbody = GetComponent<Rigidbody2D>();

            _inputActions = new AzilInputActions();
            _inputActions.Player.Move.Enable();
        }

        protected virtual void OnDisable()
        {
            _inputActions.Player.Move.Disable();
            Rigidbody.velocity = Vector3.zero;
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (other.TryGetComponent<Battler>(out var battler))
            {
                if (battler.Team != Owner.Team && Mathf.Abs(battler.transform.position.z - transform.position.z) < 1f)
                {
                    battler.Hurt(Owner.Character.GetDamageFromAttack(BaseDamage));
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y * 1.5f);

            Rigidbody.AddForce(_inputActions.Player.Move.ReadValue<Vector2>() * Speed, ForceMode2D.Force);
        }

        public void Initialize(Vector3 position, Battler owner)
        {
            transform.position = position;
            Owner = owner;

            enabled = true;
        }        
    }
}
