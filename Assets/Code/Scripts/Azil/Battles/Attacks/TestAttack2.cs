﻿using System.Collections.Generic;
using UnityEngine;
using Azil.Characters.AttackConfigs;
using MEC;

namespace Azil.Battles.Attacks
{
    public class TestAttack2 : Attack
    {
        public override bool HasValidTargets(Battle battle, AttackConfig config)
        {
            return true;
        }

        public override IEnumerator<float> AttackCoroutine(Battle battle, AttackConfig config)
        {
            yield return Timing.WaitForSeconds(1f);

            yield return Timing.WaitUntilDone(battle.SelectTileWithTeam(battle.ActiveBattler.Team, out var tile));

            tile.Result.SpriteRenderer.color = Color.green;

            yield return Timing.WaitForSeconds(1f);
        }
    }
}

