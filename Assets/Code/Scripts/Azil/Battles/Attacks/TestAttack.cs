﻿using System.Collections.Generic;
using Azil.Characters.AttackConfigs;
using MEC;

namespace Azil.Battles.Attacks
{
    public class TestAttack : Attack
    {
        public override bool HasValidTargets(Battle battle, AttackConfig config)
        {
            return true;
        }

        public override IEnumerator<float> AttackCoroutine(Battle battle, AttackConfig config)
        {
            yield return Timing.WaitForSeconds(1f);

            yield return Timing.WaitUntilDone(battle.SelectEnemy(out var battler));

            battler.Result.Hurt(100);

            yield return Timing.WaitForSeconds(1f);
        }
    }
}

