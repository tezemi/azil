﻿using Azil.Characters.AttackConfigs;
using System.Collections.Generic;

namespace Azil.Battles.Attacks
{
    public abstract class Attack
    {
        public abstract bool HasValidTargets(Battle battle, AttackConfig config);
        public abstract IEnumerator<float> AttackCoroutine(Battle battle, AttackConfig config);        
    }
}
