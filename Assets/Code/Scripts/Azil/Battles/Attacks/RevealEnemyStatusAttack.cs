﻿using System.Collections.Generic;
using Azil.Characters.AttackConfigs;
using MEC;

namespace Azil.Battles.Attacks
{
    public class RevealEnemyStatusAttack : Attack
    {
        public override bool HasValidTargets(Battle battle, AttackConfig config)
        {
            return true;
        }

        public override IEnumerator<float> AttackCoroutine(Battle battle, AttackConfig config)
        {
            yield return Timing.WaitForSeconds(1f);

            yield return Timing.WaitUntilDone(battle.SelectEnemy(out var r));
            var target = r.Result;  

            target.ApplyStatusEffect((config as RevealEnemyStatusAttackConfig).RevealedStatusEffectConfig, -1);            
            battle.BattlerStatuses[target].FadeIn();

            yield return Timing.WaitForSeconds(1f);
        }
    }
}