﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Azil.Utilities;
using Azil.UI.Battles.Attacks;
using Azil.Characters.AttackConfigs;
using MEC;

namespace Azil.Battles.Attacks
{
    public class StatusEffectWithDamageAttack : Attack
    {
        private const int BASE_DAMAGE = 10;
        private const float STATUS_EFFECT_ACCURACY = 0.075f;

        public override bool HasValidTargets(Battle battle, AttackConfig config)
        {
            var c = config as StatusEffectWithDamageAttackConfig;

            return battle.Battlers.Any(b => b.Team != battle.ActiveBattler.Team &&
                   Vector2.Distance(b.transform.position, battle.ActiveBattler.transform.position) <= c.Range);
        }

        public override IEnumerator<float> AttackCoroutine(Battle battle, AttackConfig config)
        {
            var c = config as StatusEffectWithDamageAttackConfig;

            yield return Timing.WaitForSeconds(1f);

            yield return Timing.WaitUntilDone(battle.SelectEnemyWithinRange(battle.ActiveBattler.transform.position, c.Range, out var battler));

            battle.ActiveBattler.SpriteAnimator.Play(c.WindUpSpriteAnimation);
            yield return Timing.WaitUntilDone(battle.ActiveBattler.MoveNextTo(battler.Result, true));            

            var instance = PrefabPool.Get(c.AttackInteractionPrefab);
            instance.transform.SetParent(battle.BattleCanvas.transform);
            instance.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

            if (instance.TryGetComponent<StatusEffectMeterAttackInteraction>(out var a))
            {
                battle.BattleCanvas.BattleDialog.ShowDialog($"{battle.ActiveBattler.Character.Name} goes in for a swing...");
                
                var o = new Out<float>();
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(a.PerformInteraction(o)));
                var result = o.Result;

                battle.ActiveBattler.SpriteAnimator.Play(c.SwingSpriteAnimation);

                if (Mathf.Abs(result - 0.5f) < STATUS_EFFECT_ACCURACY)
                {
                    battler.Result.ApplyStatusEffect(c.ApplyWhenAccurateStatusEffectConfig, 2);
                }

                int damage;
                if (result >= 0.5f)
                {
                    result = MathUtilities.GetRange(result, 1f, 0.5f, 0f, 1f);
                }
                else
                {
                    result = MathUtilities.GetRange(result, 0.5f, 0f, 1f, 0f);
                }

                if (result > 0.9f)
                {
                    battle.BattleCanvas.BattleDialog.ShowDialog($"INCREDIBLE!!!");
                }
                else if (result > 0.5f)
                {
                    battle.BattleCanvas.BattleDialog.ShowDialog($"Nice shot!");
                }
                else if (result > 0.25f)
                {
                    battle.BattleCanvas.BattleDialog.ShowDialog($"It was alright...");
                }
                else
                {
                    battle.BattleCanvas.BattleDialog.ShowDialog($"How pathetic.");
                }

                damage = Mathf.RoundToInt(MathUtilities.GetRangeClamped(result, 0f, 1f, BASE_DAMAGE, battle.ActiveBattler.Character.GetDamageFromAttack(BASE_DAMAGE)));

                battler.Result.Hurt(damage);
            }

            PrefabPool.Pool(instance);

            yield return Timing.WaitForSeconds(0.5f);

            battle.ActiveBattler.SpriteAnimator.Animation = battle.ActiveBattler.Character.CharacterBase.CharactersSprites.BattleIdle;

            yield return Timing.WaitUntilDone(battle.ActiveBattler.MoveBackToTile());
        }
    }
}

