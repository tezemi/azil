﻿using UnityEngine;

namespace Azil.Battles.AudioVisualization
{
    public class NoiseToPosition : MonoBehaviour
    {
        public float Size = 1f;
        public float Intensity = 1f;
        private float _seed;

        protected virtual void OnEnable()
        {
            _seed = transform.GetSiblingIndex() * 1000;
        }

        protected virtual void FixedUpdate()
        {
            transform.position = Vector3.zero + new Vector3
            (
                (Mathf.PerlinNoise1D(Mathf.Sin(_seed + Time.fixedTime * Intensity)) - 0.5f) * Size,
                (Mathf.PerlinNoise1D(Mathf.Cos(_seed + Time.fixedTime * Intensity)) - 0.5f) * Size,
                transform.position.y
            );
        }
    }
}
