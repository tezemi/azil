﻿using UnityEngine;

namespace Azil.Battles.AudioVisualization
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioVisualizer : MonoBehaviour
    {
        public AudioSource AudioSource { get; set; }
        private float _amplitudeHighest;
        private float _amplitudeHighestBuffer;
        public float[] FrequencyBandBuffers => _frequencyBandBuffers;
        public float[] NormalizedFrequencyBandBuffers => _normalizedFrequencyBandBuffers;
        private readonly float[] _samples = new float[512];        
        private readonly float[] _frequencyBands = new float[8];
        private readonly float[] _frequencyBandBuffers = new float[8];
        private readonly float[] _frequencyBandHighestValues = new float[8];
        private readonly float[] _normalizedFrequencyBands = new float[8];
        private readonly float[] _normalizedFrequencyBandBuffers = new float[8];
        private const float BUFFER_DECREASE_AMOUNT = 0.0075f;

        protected virtual void Awake()
        {
            AudioSource = GetComponent<AudioSource>();
        }

        protected virtual void Update()
        {
            // Get spectrum data
            AudioSource.GetSpectrumData(_samples, 0, FFTWindow.Blackman);

            // Set frequency bands
            var count = 0;
            for (var i = 0; i < _frequencyBands.Length; i++)
            {
                var average = 0f;
                var sampleCount = (int)Mathf.Pow(2, i) * 2;

                if (sampleCount == 7)
                {
                    sampleCount += 2;
                }

                for (var j = 0; j < sampleCount; j++)
                {
                    average += _samples[count] * (count + 1);
                    count++;
                }

                average /= count;

                _frequencyBands[i] = average;
            }

            // Set highest values
            for (var i = 0; i < _frequencyBands.Length; i++)
            {
                if (_frequencyBands[i] > _frequencyBandHighestValues[i])
                {
                    _frequencyBandHighestValues[i] = _frequencyBands[i];
                }

                _normalizedFrequencyBands[i] = _frequencyBands[i] / _frequencyBandHighestValues[i];
                _normalizedFrequencyBandBuffers[i] = _frequencyBandBuffers[i] / _frequencyBandHighestValues[i];
            }
        }

        protected virtual void FixedUpdate()
        {
            for (var i = 0; i < _frequencyBands.Length; i++)
            {
                if (_frequencyBands[i] > _frequencyBandBuffers[i])
                {
                    _frequencyBandBuffers[i] = _frequencyBands[i];
                }
                else if (_frequencyBands[i] < _frequencyBandBuffers[i])
                {
                    _frequencyBandBuffers[i] -= BUFFER_DECREASE_AMOUNT;
                }
            }
        }

        public float GetAmplitude()
        {
            var currentAmplitude = 0f;

            for (var i = 0; i < _frequencyBands.Length; i++)
            {
                currentAmplitude += _frequencyBands[i];
            }

            if (currentAmplitude > _amplitudeHighest)
            {
                _amplitudeHighest = currentAmplitude;
            }

            return currentAmplitude / _amplitudeHighest;
        }

        public float GetAmplitudeBuffer()
        {
            var currentAmplitudeBuffer = 0f;

            for (var i = 0; i < _frequencyBandBuffers.Length; i++)
            {
                currentAmplitudeBuffer += _frequencyBandBuffers[i];
            }

            if (currentAmplitudeBuffer > _amplitudeHighestBuffer)
            {
                _amplitudeHighestBuffer = currentAmplitudeBuffer;
            }

            return currentAmplitudeBuffer / _amplitudeHighestBuffer;
        }
    }
}
