﻿using MyBox;
using UnityEngine;

namespace Azil.Battles.AudioVisualization
{
    public class SpriteColorFromBandBuffer : MonoBehaviour
    {
        public bool UseAmplitude;
        [ConditionalField(nameof(UseAmplitude), true)]
        public int Buffer;
        public Gradient Gradient;
        public AudioVisualizer AudioVisualizer;        
        public SpriteRenderer SpriteRenderer { get; private set; }

        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected virtual void Update()
        {
            SpriteRenderer.color = Gradient.Evaluate(AudioVisualizer.GetAmplitudeBuffer());
        }
    }
}
