﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Azil.Battles.AudioVisualization
{
    public class TimedAudioEvents : MonoBehaviour
    {
        public TimedAudioEvent[] Events;
        public AudioSource AudioSource;
        private readonly HashSet<TimedAudioEvent> _firedEvents = new HashSet<TimedAudioEvent>();

        protected virtual void Update()
        {
            foreach (var e in Events)
            {
                if (_firedEvents.Contains(e))
                    continue;

                if (AudioSource.time > e.Time)
                {
                    e.Event.Invoke();
                    _firedEvents.Add(e);
                }
            }
        }

        [Serializable]
        public class TimedAudioEvent
        {
            public float Time;
            public UnityEvent Event;
        }
    }
}
