﻿using MEC;
using MyBox;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

namespace Azil.Battles.AudioVisualization
{
    [RequireComponent(typeof(SpriteShapeRenderer))]
    public class SpriteShapeColorFromAudioVisualizer : MonoBehaviour
    {
        public bool UseAmplitude;
        public bool UseAlpha;
        [ConditionalField(nameof(UseAmplitude), true)]
        public int Buffer;
        public Gradient Gradient;
        public AudioVisualizer AudioVisualizer;        
        public SpriteShapeRenderer SpriteShapeRenderer { get; private set; }

        protected virtual void Awake()
        {
            SpriteShapeRenderer = GetComponent<SpriteShapeRenderer>();
        }

        protected virtual void Update()
        {
            var a = SpriteShapeRenderer.color.a;

            if (UseAmplitude)
                SpriteShapeRenderer.color = Gradient.Evaluate(AudioVisualizer.GetAmplitudeBuffer());
            else
                SpriteShapeRenderer.color = Gradient.Evaluate(AudioVisualizer.NormalizedFrequencyBandBuffers[Buffer - 1]);

            if (!UseAlpha)
                SpriteShapeRenderer.color = new Color(SpriteShapeRenderer.color.r, SpriteShapeRenderer.color.g, SpriteShapeRenderer.color.b, a);
        }

        public void FadeIn()
        {
            Timing.RunCoroutine(FadeInCoroutine(), Segment.FixedUpdate);

            IEnumerator<float> FadeInCoroutine()
            {
                while (SpriteShapeRenderer.color.a != 1f)
                {
                    var c = SpriteShapeRenderer.color;
                    c.a = Mathf.MoveTowards(c.a, 1f, 0.24f);
                    SpriteShapeRenderer.color = c;

                    yield return Timing.WaitForOneFrame;
                }
            }
        }

        public void FadeOut()
        {
            Timing.RunCoroutine(FadeOutCoroutine(), Segment.FixedUpdate);

            IEnumerator<float> FadeOutCoroutine()
            {
                while (SpriteShapeRenderer.color.a != 0f)
                {
                    var c = SpriteShapeRenderer.color;
                    c.a = Mathf.MoveTowards(c.a, 0f, 0.24f);
                    SpriteShapeRenderer.color = c;

                    yield return Timing.WaitForOneFrame;
                }
            }
        }
    }
}
