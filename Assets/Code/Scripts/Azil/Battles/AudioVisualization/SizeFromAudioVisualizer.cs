﻿using UnityEngine;
using MyBox;

namespace Azil.Battles.AudioVisualization
{
    public class SizeFromAudioVisualizer : MonoBehaviour
    {
        public bool UseAmplitude;
        [ConditionalField(nameof(UseAmplitude), true)]
        public int Buffer;
        [ConditionalField(nameof(UseAmplitude), true)]
        public Vector3 MinScale = Vector3.one;
        [ConditionalField(nameof(UseAmplitude), true)]
        public Vector3 MaxScale = Vector3.one;
        public AudioVisualizer AudioVisualizer;

        protected virtual void Update()
        {
            if (UseAmplitude)
            {
                transform.localScale = MinScale + ((MaxScale - MinScale) * AudioVisualizer.GetAmplitude());
            }
            else
            {
                transform.localScale = MinScale + ((MaxScale - MinScale) * AudioVisualizer.NormalizedFrequencyBandBuffers[Buffer - 1]);
            }            
        }
    }
}
