﻿using Azil.Utilities;
using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.Windows;

namespace Azil.Battles.MusicIntegration
{
    public class MP3Wrangler : MonoBehaviour
    {
        public AudioSource AudioSource { get; private set; }
        public SpriteShapeRenderer SpriteShapeRenderer;
        public SpriteRenderer[] Bars;

        public float[] _samples = new float[512];

        protected virtual void Awake()
        {
            AudioSource = GetComponent<AudioSource>();

            Bars = GetComponentsInChildren<SpriteRenderer>();
        }

        protected virtual void Update()
        {
            AudioSource.GetSpectrumData(_samples, 0, FFTWindow.Blackman);

            //var c = SpriteShapeRenderer.color;
            //SpriteShapeRenderer.color = new Color(c.r, c.g, c.b, );

            //SpriteShapeRenderer.sharedMaterial.SetFloat("_CoolAlpha", MathUtilities.GetRange(Average(), 0f, 0.005f, 0.5f, 1f));

            //Debug.Log(Average() * 1);
            //SpriteShapeRenderer.sharedMaterial.SetFloat("_ScrollSpeed", Average() * 1);

            var i = 0;
            foreach (var f in _samples)
            {
                var bar = Bars[Mathf.RoundToInt(MathUtilities.GetRange(i, 0, 511, 0, Bars.Length - 1))];

                bar.transform.localScale = new Vector3
                (
                    bar.transform.localScale.x,
                    MathUtilities.GetRange(_samples[i], 0f, 0.005f, 0.1f, 3.25f),
                    bar.transform.localScale.z
                );

                i++;
            }
        }

        private float Average()
        {
            var avg = 0f;

            foreach (var f in _samples)
            {
                avg += f;
            }

            avg /= _samples.Length;

            return avg;
        }

    }
}
