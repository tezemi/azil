﻿using UnityEngine;

namespace Azil.Battles
{
    [RequireComponent(typeof(Camera))]
    public class BattleCamera : MonoBehaviour
    {
        public GameObject Focus;
        public Vector3? Point { get; set; }
        public float DistanceFromFocus { get; set; }
        public Camera Camera { get; private set; }
        private const float SPEED = 0.16f;
        private const float HOVER_INTENSITY = 0.1f;
        
        protected virtual void Awake()
        {           
            DistanceFromFocus = transform.position.z;
            Camera = GetComponent<Camera>();
        }

        protected virtual void FixedUpdate()
        {
            if (Focus == null) 
                return;

            Vector3 pos;
            if (Point.HasValue)
            {
                pos = new Vector3(Point.Value.x, Point.Value.y + Mathf.Sin(Time.time) * HOVER_INTENSITY, Point.Value.z + DistanceFromFocus);
            }
            else
            {
                pos = new Vector3(Focus.transform.position.x, Focus.transform.position.y + Mathf.Sin(Time.time) * HOVER_INTENSITY, Focus.transform.position.z + DistanceFromFocus);
            }

            transform.position = Vector3.Lerp(transform.position, pos, SPEED);
        }
    }
}
