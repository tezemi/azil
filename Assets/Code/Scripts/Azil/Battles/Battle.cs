﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using Azil.Utilities;
using Azil.Characters;
using Azil.UI.Battles;
using Azil.Battles.Attacks;
using Azil.Characters.AttackConfigs;
using Azil.Battles.ScriptedSequences;
using MEC;
using MyBox;
using Random = UnityEngine.Random;

namespace Azil.Battles
{
    public class Battle
    {
        public int Turn { get; protected set; }
        public int Round { get; protected set; }
        public int CurrentBattlerIndex { get; protected set; }
        public Battler ActiveBattler { get; protected set; }
        public Battler[] Battlers { get; private set; }
        public Tile[] Tiles { get; private set; }
        public GameObject RangeSelectionVisual { get; private set; }
        public BattleCanvas BattleCanvas { get; private set; }
        public WorldCanvas WorldCanvas { get; private set; }
        public BattleCamera BattleCamera { get; private set; }
        public BattleScriptedSequence BattleScriptedSequence { get; private set; }
        public Dictionary<Battler, BattlerStatus> BattlerStatuses { get; } = new Dictionary<Battler, BattlerStatus>();
        public Dictionary<Battler, StatusEffectLayoutGroup> StatusEffectLayoutGroups = new Dictionary<Battler, StatusEffectLayoutGroup>();
        public bool HasScriptedSequence => BattleScriptedSequence != null;
        private CoroutineHandle _dodgeDefendCoroutine;
        private AzilInputActions _dodgeDefendInputActions;
        private readonly List<Attack> _attacks = new List<Attack>();

        public Action<Battler> OnTurnStart { get; set; }

        public Battler LastSelectedTarget { get; private set; }

        public Battler FocusedBattler
        {
            get
            {
                if (BattleCamera.Focus == null)
                    return null;

                return BattleCamera.Focus.GetComponent<Battler>();
            }
            set
            {
                BattleCamera.Focus = value.gameObject;
            }
        }

        public Battle(Battler[] battlers, Tile[] tiles, GameObject rangeSelectionVisual, BattleCanvas battleCanvas, WorldCanvas worldCanvas, BattleCamera battleCamera)
        {
            Battlers = battlers;
            Tiles = tiles;
            RangeSelectionVisual = rangeSelectionVisual;
            BattleCanvas = battleCanvas;
            WorldCanvas = worldCanvas;
            BattleCamera = battleCamera;            
        }

        public Battle(Battler[] battlers, Tile[] tiles, GameObject rangeSelectionVisual, BattleCanvas battleCanvas, WorldCanvas worldCanvas, BattleCamera battleCamera, BattleScriptedSequence battleScriptedSequence)
        {
            Battlers = battlers;
            Tiles = tiles;
            RangeSelectionVisual = rangeSelectionVisual;
            BattleCanvas = battleCanvas;
            WorldCanvas = worldCanvas;
            BattleCamera = battleCamera;
            BattleScriptedSequence = battleScriptedSequence;

            if (HasScriptedSequence) 
            {
                BattleScriptedSequence.Battle = this;
            }            
        }

        protected Team GetWinningTeam()
        {
            Team lastTeam = null;
            foreach (Battler battler in Battlers)
            {
                if (battler.IsDeadOrIncapacitated) 
                    continue;

                if (lastTeam == null)
                {
                    lastTeam = battler.Character.Team;
                }
                else if (lastTeam != battler.Character.Team)
                {
                    return null;
                }
            }

            return lastTeam;
        }

        protected IEnumerator<float> BattleLoop()
        {
            BattleCanvas.Canvas.enabled = false;

            foreach (var battler in Battlers)
            {
                battler.Battle = this;

                var status = WorldCanvas.CreateBattlerStatus(battler.gameObject, battler.CurrentHealth, battler.CurrentSpecial);
                BattlerStatuses.Add(battler, status);

                var statusEffectLayoutGroup = WorldCanvas.CreateStatusEffectsLayoutGroupPrefab(battler.gameObject);
                StatusEffectLayoutGroups.Add(battler, statusEffectLayoutGroup);

                if (!battler.Team.IsPlayerTeam)
                    status.Hide();

                battler.OnHurt += (damage) =>
                {
                    status.SetStatus(battler.CurrentHealth, battler.CurrentSpecial);
                };
            }          

            // Determine battler turn order
            NewRound();

            BattleCanvas.TurnOrder.Initialize(Battlers.Select(b => b.Character.CharacterBase.Icon).ToArray());

            yield return Timing.WaitForOneFrame;

            // Move battlers to starting tiles
            var teams = Battlers.Select(b => b.Team).Distinct().ToArray();
            foreach (var team in teams)
            {
                var tiles = Tiles.Where(t => t.Team == team).OrderBy(t => t.PartyIndex).ToArray();
                var battlers = Battlers.Where(b => b.Team == team).ToArray();

                for (var i = 0; i < battlers.Length; i++)
                {
                    var battler = battlers[i];

                    var tile = tiles.FirstOrDefault(t => t.BattlerOccupying == null);

                    if (tile != null)
                    {
                        battler.MoveToTile(tile);                        
                    }
                    else
                    {
                        throw new Exception($"Couldn't find valid tile for battler {battler.Character.Name}.");
                    }
                }
            }

            yield return Timing.WaitUntilDone(new WaitUntil(() => SceneManager.sceneCount <= 1));
            BattleCanvas.Canvas.enabled = true;

            // TODO: Replace this with battle specific text
            BattleCanvas.BattleDialog.ShowDialog($"Time for a battle!");
            yield return Timing.WaitForSeconds(2f);

            // Central battle loop. Keeps looping until no other teams have living battlers
            while (GetWinningTeam() == null)
            {
                ActiveBattler = Battlers[CurrentBattlerIndex];

                BattleCanvas.TurnOrder.MoveIndicator(CurrentBattlerIndex);

                // As long as the battler isn't dead or incapacitated
                if (!ActiveBattler.IsDeadOrIncapacitated)
                {
                    switch (Random.Range(0, 4))
                    {
                        case 0:
                            BattleCanvas.BattleDialog.ShowDialog($"It's {ActiveBattler.Character.Name}'s turn!");
                            break;
                        case 1:
                            BattleCanvas.BattleDialog.ShowDialog($"{ActiveBattler.Character.Name} is up next.");
                            break;
                        case 2:
                            BattleCanvas.BattleDialog.ShowDialog($"It's {ActiveBattler.Character.Name}'s time to shine. ");
                            break;
                        case 3:
                            BattleCanvas.BattleDialog.ShowDialog($"Go {ActiveBattler.Character.Name}!");
                            break;
                    }
 
                    FocusedBattler = ActiveBattler;

                    yield return Timing.WaitForSeconds(2f);

                    if (OnTurnStart != null)
                        OnTurnStart(ActiveBattler);

                    if (Turn == 0 && HasScriptedSequence)
                        yield return Timing.WaitUntilDone(Timing.RunCoroutine(BattleScriptedSequence.OnBattleStart(), Segment.FixedUpdate));

                    yield return Timing.WaitForOneFrame;

                    // This can happening because of OnTurnStart effects
                    if (ActiveBattler.IsDeadOrIncapacitated)
                    {
                        yield return Timing.WaitForSeconds(0.5f);

                        BattleCanvas.BattleDialog.ShowDialog($"Oh... Youch... Nevermind.");

                        yield return Timing.WaitForSeconds(2f);
                    }
                    else
                    {
                        // Is this battler one the player controls, or the computer?
                        if (ActiveBattler.Character.Team.IsPlayerTeam)
                        {
                            getBattleOption:

                            if (HasScriptedSequence)
                                yield return Timing.WaitUntilDone(Timing.RunCoroutine(BattleScriptedSequence.OnTurnStart(), Segment.FixedUpdate));

                            WorldCanvas.BattleOptionsLayoutGroup.Show
                            (
                                true,
                                BattlerHasAvailableTiles(ActiveBattler),
                                false,
                                CanBattlerAssist(ActiveBattler),
                                false
                            );

                            WorldCanvas.BattleOptionsLayoutGroup.transform.position = ActiveBattler.transform.position + Vector3.down * 1.5f;

                            BattleCanvas.BattleDialog.ShowDialog($"What will {ActiveBattler.Character.Name} do?");

                            yield return Timing.WaitUntilDone(WorldCanvas.BattleOptionsLayoutGroup.GetSelectedOption());
                            WorldCanvas.BattleOptionsLayoutGroup.Hide();

                            var index = WorldCanvas.BattleOptionsLayoutGroup.SelectedOptionIndex;
                            if (index == 0)
                            {
                                BattleCanvas.BattleDialog.ShowDialog($"Time to attack!");

                                _attacks.Clear();
                                
                                foreach (var a in ActiveBattler.Character.Attacks)
                                {
                                    _attacks.Add(GetAttackFromConfig(a));
                                }

                                var attackMenuOptions = ActiveBattler.Character.Attacks.Select(a =>
                                {
                                    AttackSelection.AttackMenuOption amo;
                                    if (a.SpecialCost > ActiveBattler.CurrentSpecial)
                                    {
                                        amo = new AttackSelection.AttackMenuOption
                                        (
                                            a.Name,
                                            a.Description,
                                            "Not enough SP!",
                                            a.SpecialCost,
                                            false
                                        );
                                    }
                                    else if (!_attacks[ActiveBattler.Character.Attacks.IndexOf(a)].HasValidTargets(this, a))
                                    {
                                        amo = new AttackSelection.AttackMenuOption
                                        (
                                            a.Name,
                                            a.Description,
                                            "No valid targets!",
                                            a.SpecialCost,
                                            false
                                        );
                                    }
                                    else
                                    {
                                        return new AttackSelection.AttackMenuOption
                                        (
                                            a.Name,
                                            a.Description,
                                            a.SpecialCost,
                                            true
                                        );
                                    }

                                    return amo;
                                });

                                BattleCanvas.AttackSelection.Show(attackMenuOptions.ToArray());

                                if (HasScriptedSequence)
                                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(BattleScriptedSequence.OnAttackSelected(), Segment.FixedUpdate));

                                yield return Timing.WaitUntilDone(BattleCanvas.AttackSelection.GetAttackIndex(out Out<BoxedInt> selection));

                                var attackIndex = selection.Result;

                                if (attackIndex == -1)
                                {
                                    BattleCanvas.AttackSelection.Hide();
                                    goto getBattleOption;
                                }

                                var attackConfig = ActiveBattler.Attacks[attackIndex];
                                var attack = _attacks[attackIndex];

                                ActiveBattler.Character.CurrentSpecial -= attackConfig.SpecialCost;
                                if (ActiveBattler.Character.CurrentSpecial < 0)
                                    ActiveBattler.Character.CurrentSpecial = 0;

                                BattlerStatuses[ActiveBattler].SetStatus(ActiveBattler.CurrentHealth, ActiveBattler.CurrentSpecial);

                                BattleCanvas.AttackSelection.Hide();

                                BattleCanvas.BattleDialog.ShowDialog($"{ActiveBattler.Character.Name} attacks!!!");

                                if (HasScriptedSequence)
                                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(BattleScriptedSequence.OnBeforeAttackPerformed(ActiveBattler, attack)));
                                        
                                yield return Timing.WaitUntilDone(Timing.RunCoroutine(attack.AttackCoroutine(this, attackConfig), Segment.FixedUpdate));
                            }
                            else if (index == 1)
                            {
                                BattleCanvas.BattleDialog.ShowDialog($"Move where?");

                                var oldTile = ActiveBattler.OccupyingTile;
                                yield return Timing.WaitUntilDone
                                (
                                    SelectTile(Tiles.Where(t => t.Team == ActiveBattler.Team && t != ActiveBattler.OccupyingTile), out Out<Tile> outResult)
                                );

                                if (outResult.Result == null)
                                {
                                    goto getBattleOption;
                                }

                                var isMovingUpOrDown = oldTile.transform.position.x == outResult.Result.transform.position.x;
                                var isMovingBackwards = oldTile.transform.position.x > outResult.Result.transform.position.x;

                                if (isMovingUpOrDown)
                                {
                                    BattleCanvas.BattleDialog.ShowDialog($"{ActiveBattler.Character.CharacterBase.Name} finds a new position.");
                                }
                                else
                                {
                                    BattleCanvas.BattleDialog.ShowDialog
                                    (
                                        isMovingBackwards ?
                                        $"{ActiveBattler.Character.CharacterBase.Name} falls back." :
                                        $"{ActiveBattler.Character.CharacterBase.Name} steps closer."
                                    );

                                    ActiveBattler.SpriteAnimator.Play
                                    (
                                        isMovingBackwards ?
                                        ActiveBattler.Character.CharacterBase.CharactersSprites.BattleMoveBackward :
                                        ActiveBattler.Character.CharacterBase.CharactersSprites.BattleMoveForward
                                    );
                                }

                                yield return Timing.WaitUntilDone(ActiveBattler.MoveToTile(outResult.Result));

                                ActiveBattler.SpriteAnimator.Play(ActiveBattler.Character.CharacterBase.CharactersSprites.BattleIdle);

                                yield return Timing.WaitForSeconds(1f);
                            }
                            else if (index == 2)
                            {
                                // INVENTORY
                            }
                            else if (index == 3)
                            {
                                // ASSIST 

                                yield return Timing.WaitUntilDone
                                (
                                    SelectBattler(Battlers.Where(b => b.Team == ActiveBattler.Team && b.Incapacitated && !b.Dead), out Out<Battler> o)
                                );

                                var battlerToRevive = o.Result;

                                yield return Timing.WaitUntilDone(ActiveBattler.MoveToPoint(battlerToRevive.transform.position + Vector3.right * 2f));

                                yield return Timing.WaitForSeconds(1f);

                                battlerToRevive.Revive();

                                ActiveBattler.MoveToTile(ActiveBattler.OccupyingTile);

                                yield return Timing.WaitForSeconds(1f);
                            }
                        }
                        else
                        {
                            WorldCanvas.BattleOptionsLayoutGroup.Hide();

                            if (HasScriptedSequence)
                                yield return Timing.WaitUntilDone(Timing.RunCoroutine(BattleScriptedSequence.OnTurnStart(), Segment.FixedUpdate));

                            if (true)
                            {
                                BattleCanvas.BattleDialog.ShowDialog($"{ActiveBattler.Character.Name} is going to attack!");

                                yield return Timing.WaitForSeconds(1.5f);

                                var attackConfig = ActiveBattler.Attacks.GetRandom();
                                var attack = GetAttackFromConfig(attackConfig);

                                BattlerStatuses[ActiveBattler].FadeOut();
                                StatusEffectLayoutGroups[ActiveBattler].FadeOut();

                                yield return Timing.WaitForSeconds(0.25f);

                                yield return Timing.WaitUntilDone(Timing.RunCoroutine(attack.AttackCoroutine(this, attackConfig)));
                                yield return Timing.WaitForSeconds(0.25f);

                                if (ActiveBattler.StatusRevealed)
                                    BattlerStatuses[ActiveBattler].FadeIn();

                                StatusEffectLayoutGroups[ActiveBattler].FadeIn();
                            }

                            yield return Timing.WaitForSeconds(1f);
                        }
                    }
                }

                if (HasScriptedSequence)
                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(BattleScriptedSequence.OnTurnEnd(), Segment.FixedUpdate));

                Turn++;
                CurrentBattlerIndex++;
                if (CurrentBattlerIndex >= Battlers.Length)
                {
                    NewRound();

                    BattleCanvas.TurnOrder.UpdateOrder(Battlers.Select(b => b.Character.CharacterBase.Icon).ToArray());

                    yield return Timing.WaitForOneFrame;
                }
            }

            if (GetWinningTeam().IsPlayerTeam)
            {
                var playerBattlers = Battlers.Where(b => b.Team.IsPlayerTeam);
                var center = Vector3.zero;
                foreach (var battler in playerBattlers)
                {
                    center += battler.transform.position;
                }

                BattleCanvas.BattleDialog.ShowDialog($"You win!!!");

                // play victory music

                // show win dialog

                // show character panels?

                // when done, player can click continue
            }
            else
            {
                var deadBattlers = Battlers.Where(b => b.Team.IsPlayerTeam);
                var center = Vector3.zero;
                foreach (var battler in deadBattlers)
                {
                    if (!battler.Dead)
                        battler.Die();

                    center += battler.transform.position;
                }

                center = center / deadBattlers.Count();

                BattleCamera.Point = center;
                BattleCanvas.BattleDialog.ShowDialog($"Everyone is out cold...");
            }
                        
            void NewRound()
            {
                Round++;
                CurrentBattlerIndex = 0;
                Battlers = Battlers.OrderBy(b => b.Character.Speed)             // Highest speed goes first
                                   .ThenBy(b => b.Character.Level)              // If speed is the same, highest level goes first
                                   .ThenBy(b => b.transform.GetSiblingIndex())  // If that's also the same, then it's just whoever
                                   .Reverse().ToArray();                        // got lucky enough to be higher up in the scene

                foreach (var battler in Battlers)
                {
                    for (var i = 0; i < battler.StatusEffects.Count; i++)
                    {
                        var statusEffect = battler.StatusEffects[i];
                        if (statusEffect.Duration <= -1)
                            continue;

                        statusEffect.Duration--;

                        if (statusEffect.Duration <= 0)
                        {
                            battler.RemoveStatusEffect(statusEffect); // ???
                        }
                        else
                        {
                            StatusEffectLayoutGroups[battler].UpdateStatusEffectDuration(statusEffect.Config.Icon, statusEffect.Duration);
                        }
                    }
                }
            }
        }

        public void StartBattle()
        {
            Timing.RunCoroutine(BattleLoop(), Segment.FixedUpdate);
        }

        public CoroutineHandle StartDodgeDefend(Battler battler)
        {
            _dodgeDefendCoroutine = Timing.RunCoroutine(DodgeDefendCoroutine(), Segment.Update);

            return _dodgeDefendCoroutine;

            IEnumerator<float> DodgeDefendCoroutine()
            {
                if (battler.IsDeadOrIncapacitated)
                    yield break;

                _dodgeDefendInputActions = new AzilInputActions();
                _dodgeDefendInputActions.UI.Submit.Enable();
                _dodgeDefendInputActions.UI.Cancel.Enable();

                BattleCanvas.DodgeDefend.Show();

                yield return Timing.WaitForOneFrame;
                yield return Timing.WaitUntilDone(new WaitUntil(() => 
                             _dodgeDefendInputActions.UI.Submit.triggered || _dodgeDefendInputActions.UI.Cancel.triggered ||
                             BattleCanvas.DodgeDefend.DodgeTriggered || BattleCanvas.DodgeDefend.DefendTriggered));

                if (_dodgeDefendInputActions.UI.Submit.triggered || BattleCanvas.DodgeDefend.DodgeTriggered)
                {
                    BattleCanvas.DodgeDefend.Hide();

                    battler.Dodging = true;
                    battler.SpriteRenderer.color = Color.grey;

                    var dodgeScale = Vector3.one * 0.87f;
                    while (battler.transform.localScale != dodgeScale)
                    {
                        battler.transform.localScale = Vector3.MoveTowards(battler.transform.localScale, dodgeScale, 0.02f);

                        yield return Timing.WaitForOneFrame;
                    }

                    yield return Timing.WaitForSeconds(0.5f);

                    while (battler.transform.localScale != Vector3.one)
                    {
                        battler.transform.localScale = Vector3.MoveTowards(battler.transform.localScale, Vector3.one, 0.02f);

                        yield return Timing.WaitForOneFrame;
                    }
                }
                else if (_dodgeDefendInputActions.UI.Cancel.triggered || BattleCanvas.DodgeDefend.DefendTriggered)
                {
                    BattleCanvas.DodgeDefend.Hide();

                    battler.Defending = true;
                    battler.SpriteRenderer.color = Color.grey;

                    yield return Timing.WaitForSeconds(3f);
                }

                StopDodgeDefend(battler);
            }
        }

        public void StopDodgeDefend(Battler battler)
        {
            battler.Dodging = false;
            battler.Defending = false;
            battler.SpriteRenderer.color = Color.white;

            _dodgeDefendInputActions.UI.Submit.Disable();
            _dodgeDefendInputActions.UI.Cancel.Disable();

            BattleCanvas.DodgeDefend.Hide();

            battler.transform.localScale = Vector3.one;

            Timing.KillCoroutines(_dodgeDefendCoroutine);
        }

        public void UpdateBattleOptionsBasedOnBattlerStatus()
        {
            if (!ActiveBattler.Team.IsPlayerTeam || !WorldCanvas.BattleOptionsLayoutGroup.AssistButton.isActiveAndEnabled)
                return;

            var foundTeammateToAssist = false;
            foreach (var battler in Battlers)
            {
                if (battler.Team == ActiveBattler.Team && battler.Incapacitated && !battler.Dead)
                {
                    foundTeammateToAssist = true;
                }
            }

            if (!foundTeammateToAssist)
            {
                WorldCanvas.BattleOptionsLayoutGroup.AssistButton.gameObject.SetActive(false);
            }
        }

        public bool CanBattlerAssist(Battler battler)
        {
            foreach (var b in Battlers)
            {
                if (b == battler)
                    continue;

                if (b.Team == battler.Team && b.Incapacitated && !b.Dead)
                {
                    return true;
                }
            }

            return false;
        }

        public void ShowRangeSelectionVisual(Vector3 origin, float range)
        {
            RangeSelectionVisual.transform.position = origin;
            RangeSelectionVisual.transform.localScale = Vector3.one * range;
            RangeSelectionVisual.SetActive(true);
        }

        public void HideRangeSelectionVisual()
        {
            RangeSelectionVisual.SetActive(false);
        }

        /////////////////////////////////////////////////////////////////////////////
        // Selecting Tiles and Battlers /////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////

        public CoroutineHandle SelectBattler(IEnumerable<Battler> battlers, out Out<Battler> selectedBattler)
        {
            var b = new Out<Battler>();
            selectedBattler = b;

            return Timing.RunCoroutine(SelectBattlerCoroutine(), Segment.Update);

            IEnumerator<float> SelectBattlerCoroutine()
            {
                var input = new AzilInputActions();
                input.UI.Navigate.Enable();
                input.UI.Submit.Enable();
                input.UI.Click.Enable();                             

                var selectedBattler = battlers.First();

                WorldCanvas.BattleSelector.Show(selectedBattler.transform.position);

                foreach (var battler in Battlers.Where(b => !battlers.Contains(b)))
                {
                    SetBattlerAlpha(battler, 0.5f);
                }

                Vector2 center = Vector2.zero;
                foreach (Battler battler in battlers)
                {
                    var entry = new EventTrigger.Entry();
                    entry.eventID = EventTriggerType.PointerEnter;
                    entry.callback.AddListener((eventData) => { SelectBattler(battler); });

                    battler.EventTrigger.triggers.Add(entry);

                    center += (Vector2)battler.transform.position;
                }

                center = center / battlers.Count();

                BattleCamera.Point = center;

                SelectBattler(selectedBattler);

                while (true)
                {
                    yield return Timing.WaitForOneFrame;
                    yield return Timing.WaitUntilDone(new WaitUntil(
                                                        () => input.UI.Navigate.ReadValue<Vector2>() != Vector2.zero ||
                                                        input.UI.Submit.triggered ||
                                                        input.UI.Click.triggered));

                    if (input.UI.Submit.triggered || input.UI.Click.triggered)
                    {
                        break;
                    }

                    if (battlers.Count() == 1)
                    {
                        continue;
                    }

                    var value = input.UI.Navigate.ReadValue<Vector2>();

                    var battler = battlers
                        .Where(b => b != selectedBattler)
                        .MinBy(b => Vector2.Distance((Vector2)(b.transform.position - selectedBattler.transform.position), value));

                    SelectBattler(battler);

                    yield return Timing.WaitForOneFrame;
                    yield return Timing.WaitUntilDone(new WaitUntil(() => input.UI.Navigate.ReadValue<Vector2>() == Vector2.zero));
                }

                SetBattlerAlpha(selectedBattler, 1f);
                b.Result = selectedBattler;

                BattleCamera.Point = null;

                foreach (Battler battler in battlers)
                {
                    battler.EventTrigger.triggers.Clear();
                }

                foreach (var battler in Battlers.Where(b => !battlers.Contains(b)))
                {
                    SetBattlerAlpha(battler, 1f);
                }

                input.UI.Navigate.Disable();
                input.UI.Submit.Disable();
                input.UI.Click.Disable();

                WorldCanvas.BattleSelector.Hide();

                void SetBattlerAlpha(Battler battler, float alpha)
                {
                    var c = battler.SpriteRenderer.color;
                    c.a = alpha;
                    battler.SpriteRenderer.color = c;
                }

                void SelectBattler(Battler battler)
                {
                    //SetBattlerAlpha(selectedBattler, 1f);
                    selectedBattler = battler;
                    //SetBattlerAlpha(selectedBattler, 0.5f);
                    WorldCanvas.BattleSelector.Target = selectedBattler.gameObject;
                }
            }
        }

        public CoroutineHandle SelectEnemy(out Out<Battler> battler)
        {
            return SelectBattler(Battlers.Where(b => b.Team != ActiveBattler.Team), out battler);
        }

        public CoroutineHandle SelectEnemyWithinRange(Vector3 origin, float range, out Out<Battler> battler)
        {
            return SelectBattler(Battlers.Where(b => b.Team != ActiveBattler.Team && Vector2.Distance((Vector2)origin, (Vector2)b.transform.position) < range), out battler);
        }

        public bool BattlerHasAvailableTiles(Battler battler)
        {
            foreach (var tile in Tiles)
            {
                if (tile.Team == battler.Team && tile != battler.OccupyingTile)
                {
                    return true;
                }
            }

            return false;
        }

        public CoroutineHandle SelectTile(IEnumerable<Tile> tiles, out Out<Tile> selectedTile)
        {
            var t = new Out<Tile>();
            selectedTile = t;            

            return Timing.RunCoroutine(SelectTileCoroutine(), Segment.Update);

            IEnumerator<float> SelectTileCoroutine()
            {
                var input = new AzilInputActions();
                input.UI.Navigate.Enable();
                input.UI.Submit.Enable();
                input.UI.Click.Enable();
                input.UI.Cancel.Enable();

                var selectedTile = tiles.First();
                SelectTile(selectedTile);

                BattleCamera.Point = tiles.Select(t => t.transform.position).Center();

                WorldCanvas.BattleSelector.Show(selectedTile.transform.position);

                BattleCanvas.CancelButton.gameObject.SetActive(true);
                var buttonEvents = BattleCanvas.CancelButton.GetComponent<EventTrigger>();

                var cancelButtonHighlighted = false;

                var onPointerEnterEvent = new EventTrigger.Entry();
                onPointerEnterEvent.eventID = EventTriggerType.PointerEnter;
                onPointerEnterEvent.callback.AddListener((e) => { cancelButtonHighlighted = true; });
                buttonEvents.triggers.Add(onPointerEnterEvent);

                var onPointerExitEvent = new EventTrigger.Entry();
                onPointerExitEvent.eventID = EventTriggerType.PointerExit;
                onPointerExitEvent.callback.AddListener((e) => { cancelButtonHighlighted = false; });
                buttonEvents.triggers.Add(onPointerExitEvent);

                foreach (Tile tile in tiles)
                {
                    var entry = new EventTrigger.Entry();
                    entry.eventID = EventTriggerType.PointerEnter;
                    entry.callback.AddListener((eventData) => { SelectTile(tile); });

                    tile.EventTrigger.triggers.Add(entry);

                    //tile.PulseSpriteEffect.enabled = true;
                }

                foreach (var tile in Tiles.Where(t => !tiles.Contains(t)))
                {
                    SetTileAlpha(tile, 0.5f);
                }

                while (true)
                {
                    yield return Timing.WaitForOneFrame;
                    yield return Timing.WaitUntilDone(new WaitUntil(
                                                        () => input.UI.Navigate.ReadValue<Vector2>() != Vector2.zero || 
                                                        input.UI.Submit.triggered || 
                                                        input.UI.Click.triggered || 
                                                        input.UI.Cancel.triggered));

                    if (input.UI.Submit.triggered || input.UI.Click.triggered || input.UI.Cancel.triggered)
                    {
                        break;
                    }

                    var value = input.UI.Navigate.ReadValue<Vector2>();

                    var tile = tiles
                        .Where(t => t != selectedTile)
                        .MinBy(t => Vector2.Distance((Vector2)(t.transform.position - selectedTile.transform.position), value));

                    SelectTile(tile);

                    yield return Timing.WaitForOneFrame;
                    yield return Timing.WaitUntilDone(new WaitUntil(() => input.UI.Navigate.ReadValue<Vector2>() == Vector2.zero));
                }

                SetTileAlpha(selectedTile, 1f);

                if (input.UI.Cancel.triggered || (input.UI.Click.triggered && cancelButtonHighlighted))
                {
                    t.Result = null;
                }
                else
                {
                    t.Result = selectedTile;
                }

                foreach (Tile tile in tiles)
                {
                    tile.EventTrigger.triggers.Clear();

                    //tile.PulseSpriteEffect.enabled = false;
                }

                foreach (var tile in Tiles.Where(t => !tiles.Contains(t)))
                {
                    SetTileAlpha(tile, 1f);
                }

                input.UI.Navigate.Disable();
                input.UI.Submit.Disable();
                input.UI.Click.Disable();
                input.UI.Cancel.Disable();

                buttonEvents.triggers.Clear();
                BattleCanvas.CancelButton.gameObject.SetActive(false);

                WorldCanvas.BattleSelector.Hide();

                BattleCamera.Point = null;

                void SetTileAlpha(Tile tile, float alpha)
                {
                    var c = tile.SpriteRenderer.color;
                    c.a = alpha;
                    tile.SpriteRenderer.color = c;
                }

                void SelectTile(Tile tile)
                {
                    //SetTileAlpha(selectedTile, 1f);
                    selectedTile = tile;
                    //SetTileAlpha(selectedTile, 0.5f);
                    WorldCanvas.BattleSelector.Target = tile.gameObject;
                }
            }
        }

        public CoroutineHandle SelectTileWithTeam(Team team, out Out<Tile> selectedTile)
        {
            selectedTile = new Out<Tile>();

            return SelectTile(Tiles.Where(t => t.Team == team), out selectedTile);
        }

        private Attack GetAttackFromConfig(AttackConfig config)
        {
            const string attacksNamespace = "Azil.Battles.Attacks.";
            var attackImplementation = (Attack)Activator.CreateInstance(Type.GetType(attacksNamespace + config.AttackScriptTypeName));

            return attackImplementation;
        }
    }
}
