﻿using System.Collections.Generic;
using UnityEngine;
using Azil.Battles.Attacks;
using MEC;

namespace Azil.Battles.ScriptedSequences
{
    public abstract class BattleScriptedSequence : ScriptableObject
    {
        public Battle Battle { get; set; }

        public virtual IEnumerator<float> OnBattleStart() 
        {
            yield return 0f;
        }

        public virtual IEnumerator<float> OnTurnStart()
        {
            yield return 0f;
        }

        public virtual IEnumerator<float> OnTurnEnd()
        {
            yield return 0f;
        }

        public virtual IEnumerator<float> OnAttackSelected()
        {
            yield return 0f;
        }

        public virtual IEnumerator<float> OnBeforeAttackPerformed(Battler battler, Attack attack)
        {
            yield return 0f;
        }

        protected CoroutineHandle WaitForStandardTime()
        {
            return Timing.RunCoroutine(WaitForStandardTime());
            
            IEnumerator<float> WaitForStandardTime()
            {
                yield return Timing.WaitForSeconds(2f);
            }
        }

        protected CoroutineHandle ShowDialog(string dialog)
        {
            return Battle.BattleCanvas.BattleDialog.ShowDialog(dialog);
        }

        protected CoroutineHandle ShowDialog(string dialog, Battler speaker)
        {            
            Battle.BattleCanvas.BattleDialog.DialogTail.Show(Battle.BattleCamera.Camera, speaker.transform.position);

            return Battle.BattleCanvas.BattleDialog.ShowDialog(dialog);
        }

        protected void HideDialogTail()
        {
            Battle.BattleCanvas.BattleDialog.DialogTail.Hide();
        }
    }
}
