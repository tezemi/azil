﻿using System.Collections.Generic;
using UnityEngine;
using Azil.Battles.Attacks;
using Azil.Utilities;
using MEC;

namespace Azil.Battles.ScriptedSequences
{
    [CreateAssetMenu(fileName = nameof(TutorialScriptedSequence), menuName = "Azil/Battle Scripted Sequences/" + nameof(TutorialScriptedSequence))]
    public class TutorialScriptedSequence : BattleScriptedSequence
    {
        public SpriteAnimation AzealAngrySpriteAnimation;
        private int _goodGuyHealth;

        public override IEnumerator<float> OnBattleStart()
        {
            yield return Timing.WaitUntilDone(ShowDialog("Ivek: Alright, let's just cover the basics...", Battle.Battlers[1]));
            yield return Timing.WaitUntilDone(WaitForStandardTime());

            HideDialogTail();
        }

        public override IEnumerator<float> OnTurnStart()
        {
            if (Battle.Battlers[0] == Battle.ActiveBattler && Battle.Round == 1)
            {
                yield return Timing.WaitUntilDone(ShowDialog("Ivek: First, try attacking me. When it comes up, select the attack option.", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: Don't worry, take your best shot. I'll be fine.", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                HideDialogTail();
                 
                Battle.WorldCanvas.BattleOptionsLayoutGroup.MoveButton.interactable = false;
            }

            if (Battle.ActiveBattler  == Battle.Battlers[1] && Battle.Round == 1)
            {
                _goodGuyHealth = Battle.Battlers[0].CurrentHealth;

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: When you get attacked, you can either DODGE (Z) or DEFEND (X).", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: Dodging requires more precise timing, but if you manage to dodge an attack, you won't take any damage.", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: Defending is easier, but you'll still take some damage.", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: Don't worry, I won't actually hurt you <size=8>that badly.</size>", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime()); 

                HideDialogTail();
            }

            if (Battle.Battlers[0] == Battle.ActiveBattler && Battle.Round == 2)
            {
                yield return Timing.WaitUntilDone(ShowDialog("Ivek: Next, let's try moving.", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: When you have the chance, you can move from spot to spot.", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: Depending on where you're standing, different attacks might be available to you.", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                Battle.Battlers[0].SpriteAnimator.Play(AzealAngrySpriteAnimation);

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: You might also be more suspectable to enemy attacks too.", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: A lot can change depending on where you're standing!", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                yield return Timing.WaitUntilDone(ShowDialog("Azeal: You really don't need to explain to me how to fucking move around man...", Battle.Battlers[0]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                Battle.Battlers[0].SpriteAnimator.Play(Battle.Battlers[0].Character.CharacterBase.CharactersSprites.BattleIdle);

                HideDialogTail();

                Battle.WorldCanvas.BattleOptionsLayoutGroup.AttackButton.interactable = false;
                Battle.WorldCanvas.BattleOptionsLayoutGroup.MoveButton.interactable = true;
            }

            if (Battle.Battlers[1] == Battle.ActiveBattler && Battle.Round == 2)
            {
                yield return Timing.WaitUntilDone(ShowDialog("Ivek: Great, that's enough for now.", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: I think you're ready for a real battle!", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                HideDialogTail();
            }
        }

        public override IEnumerator<float> OnTurnEnd()
        {
            if (Battle.ActiveBattler == Battle.Battlers[1] && Battle.Round == 1 && Battle.Battlers[0].CurrentHealth < _goodGuyHealth)
            {
                Battle.Battlers[0].SpriteAnimator.Play(AzealAngrySpriteAnimation);

                yield return Timing.WaitUntilDone(ShowDialog("Azeal: Ow!!! Shit!!!", Battle.Battlers[0]));
                yield return Timing.WaitUntilDone(WaitForStandardTime()); 

                yield return Timing.WaitUntilDone(ShowDialog("Azeal: I thought you said you weren't going to hurt me!?", Battle.Battlers[0]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: Don't be a baby, it's just a knife.", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                yield return Timing.WaitUntilDone(ShowDialog("Ivek: I literally made the smallest cut this knife is capable of making.", Battle.Battlers[1]));
                yield return Timing.WaitUntilDone(WaitForStandardTime());

                Battle.Battlers[0].SpriteAnimator.Play(Battle.Battlers[0].Character.CharacterBase.CharactersSprites.BattleIdle);

                HideDialogTail();
            }
        }

        public override IEnumerator<float> OnAttackSelected()
        {
            yield return Timing.WaitUntilDone(ShowDialog("Ivek: Great work. Now pick an attack.", Battle.Battlers[1]));
        }

        public override IEnumerator<float> OnBeforeAttackPerformed(Battler battler, Attack attack)
        {
            yield return Timing.WaitUntilDone(ShowDialog("Ivek: Each attack is different, but usually requires you to perform some kind of input in order to maximize your damage.", Battle.Battlers[1]));
            yield return Timing.WaitUntilDone(WaitForStandardTime());

            yield return Timing.WaitUntilDone(ShowDialog("Ivek: Be sure to read about each attack so you know what you'll need to do.", Battle.Battlers[1]));
            yield return Timing.WaitUntilDone(WaitForStandardTime());

            yield return Timing.WaitUntilDone(ShowDialog("Ivek: Practice usually makes perfect.", Battle.Battlers[1]));
            yield return Timing.WaitUntilDone(WaitForStandardTime());

            HideDialogTail();
        }
    }
}
