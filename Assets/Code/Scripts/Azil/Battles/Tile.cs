﻿using UnityEngine;
using UnityEngine.EventSystems;
using Azil.Utilities;
using Azil.Characters;

namespace Azil.Battles
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(EventTrigger))]
    [RequireComponent(typeof(PulseSpriteEffect))]
    public class Tile : MonoBehaviour
    {
        public int PartyIndex;
        public FacingDirection Facing;
        public Team Team; 
        public Battler BattlerOccupying { get; set; }
        public SpriteRenderer SpriteRenderer { get; private set; }
        public EventTrigger EventTrigger { get; private set; }
        public PulseSpriteEffect PulseSpriteEffect { get; private set; }
        private Team _team;

        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
            EventTrigger = GetComponent<EventTrigger>();
            PulseSpriteEffect = GetComponent<PulseSpriteEffect>();

            SpriteRenderer.color = Team != null ? Team.Color : Color.white;
            _team = Team;
        }

        protected virtual void FixedUpdate()
        {
            if (Team != _team)
            {
                SpriteRenderer.color = Team != null ? Team.Color : Color.white;
                _team = Team;
            }
        }

        public enum FacingDirection
        {
            FacesRight,
            FacesLeft
        }
    }
}
