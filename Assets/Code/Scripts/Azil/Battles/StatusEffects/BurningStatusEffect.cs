﻿using Azil.Characters.StatusEffectConfigs;
using UnityEngine;

namespace Azil.Battles.StatusEffects
{
    public class BurningStatusEffect : StatusEffect
    {
        private Battler _battler;

        public BurningStatusEffect(StatusEffectConfig config, int duration) : base(config, duration)
        {
            // ...
        }

        public override void OnStatusEffectApplied(Battler battler)
        {
            _battler = battler;
            battler.Battle.OnTurnStart += OnTurnStart;
        }

        public override void OnStatusEffectRemoved(Battler battler)
        {
            battler.Battle.OnTurnStart -= OnTurnStart;
        }

        private void OnTurnStart(Battler battler)
        {
            if (battler == _battler)
                battler.Hurt(Mathf.RoundToInt(battler.MaxHealth * (Config as BurningStatusEffectConfig).PercentageOfMaxHealthDoneAsDamage), true);
        }
    }
}
