﻿using Azil.Characters.StatusEffectConfigs;

namespace Azil.Battles.StatusEffects
{
    public class StatusRevealedStatusEffect : StatusEffect
    {
        public StatusRevealedStatusEffect(StatusEffectConfig config, int duration) : base(config, duration)
        {
            // ...
        }

        public override void OnStatusEffectApplied(Battler battler)
        {
            battler.StatusRevealed = true;
        }

        public override void OnStatusEffectRemoved(Battler battler)
        {
            battler.StatusRevealed = false;

            battler.Battle.BattlerStatuses[battler].FadeOut();
        }
    }
}
