﻿using Azil.Characters.StatusEffectConfigs;

namespace Azil.Battles.StatusEffects
{
    public abstract class StatusEffect
    {
        public int Duration { get; set; }
        public StatusEffectConfig Config { get; private set; }

        public StatusEffect(StatusEffectConfig config, int duration)
        {
            Duration = duration;
            Config = config;
        }

        public abstract void OnStatusEffectApplied(Battler battler);
        public abstract void OnStatusEffectRemoved(Battler battler);
    }
}
