﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Azil.Utilities;
using Azil.Characters;
using Azil.Battles.StatusEffects;
using Azil.Characters.AttackConfigs;
using Azil.Characters.StatusEffectConfigs;
using MEC;
using MyBox;
using static UnityEditor.PlayerSettings;

namespace Azil.Battles
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(EventTrigger))]
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(SpriteAnimator))]
    public class Battler : MonoBehaviour
    {
        public bool Dead { get; set; }
        public bool Incapacitated { get; set; }
        public bool Dodging { get; set; }
        public bool Defending { get; set; }
        public bool StatusRevealed { get; set; }
        public Battle Battle { get; set; }
        public Action<int> OnHurt { get; set; }
        public int IncapacitateTimer { get; private set; }
        public int IncapacitateCount { get; private set; } = 1;
        public Tile OccupyingTile { get; private set; }
        public BoxCollider2D BoxCollider { get; private set; }
        public EventTrigger EventTrigger { get; private set; }
        public SpriteRenderer SpriteRenderer { get; private set; }
        public SpriteAnimator SpriteAnimator { get; private set; }
        public List<StatusEffect> StatusEffects { get; } = new List<StatusEffect>();
        public bool IsDeadOrIncapacitated => Dead || Incapacitated;
        public bool ShouldShowStatus => Team.IsPlayerTeam || StatusRevealed;
        public int MaxHealth => Character.MaxHealth;
        public int CurrentHealth => Character.CurrentHealth;
        public int CurrentSpecial => Character.CurrentSpecial;
        public Team Team => Character.Team;
        public List<AttackConfig> Attacks => Character.Attacks;
        private CoroutineHandle _incapacitateCoroutineHandle;
        private Character _character;
        private GameObject _deathTimerInstance;
        private const int INCAPACITATE_TIME = 30;
        private const float MOVE_SPEED = 0.12f;

        public Character Character
        {
            get
            {
                return _character;
            }
            set
            {
                BoxCollider.size = new Vector2
                (
                    value.CharacterBase.CharactersSprites.BattleIdle.Sprites[0].bounds.size.x,
                    value.CharacterBase.CharactersSprites.BattleIdle.Sprites[0].bounds.size.y
                );

                SpriteAnimator.Play(value.CharacterBase.CharactersSprites.BattleIdle);

                name = $"Battler ({value.CharacterBase.Name})";

                _character = value;
            }
        }

        protected virtual void Awake()
        {
            BoxCollider = GetComponent<BoxCollider2D>();
            EventTrigger = GetComponent<EventTrigger>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            SpriteAnimator = GetComponent<SpriteAnimator>();            
        }

        protected virtual void OnEnable()
        {
            if (Character != null)
            {
                SpriteAnimator.Play(Character.CharacterBase.CharactersSprites.BattleIdle);
            }
        }

        public CoroutineHandle MoveToTile(Tile tile)
        {
            if (OccupyingTile == tile)
            {
                return Timing.RunCoroutine(MoveToTileCoroutine(), Segment.FixedUpdate);                
            }                

            if (OccupyingTile != null && OccupyingTile.BattlerOccupying == this)
            {
                OccupyingTile.BattlerOccupying = null;                
            }

            if (tile.BattlerOccupying != null)
            {
                var otherBattler = tile.BattlerOccupying;
                otherBattler.MoveToTile(OccupyingTile);
            }

            tile.BattlerOccupying = this;
            OccupyingTile = tile;

            return Timing.RunCoroutine(MoveToTileCoroutine(), Segment.FixedUpdate);

            IEnumerator<float> MoveToTileCoroutine()
            {
                var pos = tile.transform.position;
                pos.y += BoxCollider.bounds.extents.y;

                SpriteRenderer.flipX = OccupyingTile.Facing == Tile.FacingDirection.FacesLeft;

                while (Vector3.Distance(transform.position, pos) > MOVE_SPEED / 2f)
                {
                    transform.position = Vector3.Lerp(transform.position, pos, MOVE_SPEED);

                    yield return Timing.WaitForOneFrame;
                }

                transform.position = pos;
            }
        }

        public CoroutineHandle MoveToPoint(Vector3 point)
        {
            return Timing.RunCoroutine(MoveToPointCoroutine(), Segment.FixedUpdate);

            IEnumerator<float> MoveToPointCoroutine()
            {
                while (Vector3.Distance(transform.position, point) > MOVE_SPEED / 2f)
                {
                    transform.position = Vector3.Lerp(transform.position, point, MOVE_SPEED);

                    yield return Timing.WaitForOneFrame;
                }

                transform.position = point;
            }
        }

        public CoroutineHandle MoveNextTo(Battler battler, bool left)
        {
            const float padding = 0.4f;
            var theirExtents = battler.BoxCollider.bounds.extents.x;
            var myExtents = BoxCollider.bounds.extents.x;

            if (left)
            {
                return MoveToPoint(battler.transform.position + Vector3.left * theirExtents + Vector3.left * myExtents + Vector3.left * padding);
            }
            else
            {
                return MoveToPoint(battler.transform.position + Vector3.right * theirExtents + Vector3.right * myExtents + Vector3.right * padding);
            }
        }

        public CoroutineHandle MoveBackToTile()
        {
            return MoveToTile(OccupyingTile);
        }

        public virtual void Heal(int health)
        {
            //GameObject instance = Instantiate(DamageTextPrefab);
            //instance.GetComponent<DamageText>().Text.color = Color.green;
            //instance.GetComponent<DamageText>().Text.text = $"+{health}";
            //instance.GetComponent<Rigidbody2D>().gravityScale = -0.25f;
            //instance.GetComponent<Rigidbody2D>().drag = 1.5f;
            //instance.transform.position = transform.position;
        }

        public virtual void Hurt(int damage, bool ignoreDefense = false)
        {
            if (IsDeadOrIncapacitated || Dodging) 
                return;

            if (!ignoreDefense)
                damage = Character.ReduceDamageFromDefense(damage);

            if (Defending)
                damage /= 2;

            if (damage <= 0)
                damage = 1;

            Character.CurrentHealth -= damage;

            Battle.WorldCanvas.CreateDamageNumber(transform.position + Vector3.up, damage);

            if (OnHurt != null)
                OnHurt(damage);

            Timing.RunCoroutine(HurtCoroutine(), Segment.FixedUpdate);
                        
            if (Character.CurrentHealth <= 0)
            {
                Character.CurrentHealth = 0;

                if (Team.IsPlayerTeam)
                    Incapacitate();
                else
                    Die();
            }

            IEnumerator<float> HurtCoroutine()
            {
                SpriteAnimator.Play(Character.CharacterBase.CharactersSprites.BattleHurt);

                for (int i = 0; i < 10; i++)
                {
                    SpriteRenderer.enabled = !SpriteRenderer.enabled;
                    yield return Timing.WaitForSeconds(0.05f);
                }

                SpriteRenderer.enabled = true;

                SpriteAnimator.Play(Character.CharacterBase.CharactersSprites.BattleIdle);
            }
        }

        public virtual void Incapacitate()
        {
            if (IsDeadOrIncapacitated) 
                return;

            Incapacitated = true;
            IncapacitateTimer = INCAPACITATE_TIME / IncapacitateCount;
            IncapacitateCount++;
            SpriteRenderer.color = Color.grey;
            SpriteAnimator.PlayOnce(Character.CharacterBase.CharactersSprites.BattleIncapacitate);

            _incapacitateCoroutineHandle = Timing.RunCoroutine(IncapacitateCoroutine(), Segment.FixedUpdate);

            IEnumerator<float> IncapacitateCoroutine()
            {
                Battle.BattlerStatuses[this].FadeOut();

                yield return Timing.WaitForSeconds(0.5f);

                var d = Battle.WorldCanvas.CreateDeathTimer(gameObject, transform.position + Vector3.up);
                _deathTimerInstance = d.gameObject;
                d.SetTime(IncapacitateTimer);

                while (Incapacitated)
                {
                    yield return Timing.WaitForSeconds(1f);

                    IncapacitateTimer--;
                    d.SetTime(IncapacitateTimer);

                    if (IncapacitateTimer <= 0)
                    {
                        Die();

                        yield break;
                    }
                }
            }
        }

        public virtual void Die()
        {
            if (Dead) 
                return;

            for (var i = 0; i < StatusEffects.Count; i++)
            {
                var statusEffect = StatusEffects[i];
                if (statusEffect.Config.KeepOnDeath)
                    continue;
                 
                RemoveStatusEffect(statusEffect);
            }

            Dead = true;
            SpriteRenderer.color = Color.black;

            Battle.UpdateBattleOptionsBasedOnBattlerStatus();
            Battle.BattlerStatuses[this].FadeOut();

            if (!SpriteAnimator.IsPlaying(Character.CharacterBase.CharactersSprites.BattleIncapacitate))
                SpriteAnimator.PlayOnce(Character.CharacterBase.CharactersSprites.BattleIncapacitate);

            if (_deathTimerInstance != null)
                PrefabPool.Pool(_deathTimerInstance);
        }

        public virtual void Revive()
        {
            Dead = false;
            Incapacitated = false;
            Character.CurrentHealth = 1;
            SpriteRenderer.color = Color.white;
            SpriteAnimator.Play(Character.CharacterBase.CharactersSprites.BattleIdle);
            PrefabPool.Pool(_deathTimerInstance);

            if (ShouldShowStatus)
                Battle.BattlerStatuses[this].FadeIn();

            Battle.BattlerStatuses[this].SetStatus(CurrentHealth, CurrentSpecial);

            Timing.KillCoroutines(_incapacitateCoroutineHandle);            
        }

        public virtual void ApplyStatusEffect(StatusEffectConfig statusEffectConfig, int duration)
        {
            foreach (var s in StatusEffects)
            {
                if (s.Config == statusEffectConfig)
                {
                    s.Duration = duration;

                    Battle.StatusEffectLayoutGroups[this].UpdateStatusEffectDuration(statusEffectConfig.Icon, duration);

                    return;
                }
            }

            var type = Type.GetType("Azil.Battles.StatusEffects." + statusEffectConfig.StatusEffectTypeName);
            var statusEffect = (StatusEffect)Activator.CreateInstance(type, new object[] { statusEffectConfig, duration });

            StatusEffects.Add(statusEffect);

            Battle.StatusEffectLayoutGroups[this].AddStatusEffect(statusEffectConfig.Icon, duration);

            statusEffect.OnStatusEffectApplied(this);
        }

        public virtual void RemoveStatusEffect(StatusEffect statusEffect)
        {
            StatusEffects.Remove(statusEffect);

            Battle.StatusEffectLayoutGroups[this].RemoveStatusEffect(statusEffect.Config.Icon);

            statusEffect.OnStatusEffectRemoved(this);
        }

        [ButtonMethod]
        public void SpillTheBeansBaby()
        {
            Debug.Log(Character.Name + $": {CurrentHealth} / {MaxHealth}");
        }
    }
}
