﻿using UnityEngine;

namespace Azil.Overworld
{
    public class PlayerController : PartyCharacter
    {
        public static PlayerController Main { get; private set; }

        protected override void Awake()
        {
            base.Awake();

            if (Main == null)
            {
                Main = this;
            }
        }

        protected override void Update()
        {
            base.Update();

            Vector3 moveTo = Vector3.zero;
            if (Input.GetKey(KeyCode.RightArrow))
            {
                moveTo += new Vector3(MaxVelocity, 0f, 0f);
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                moveTo += new Vector3(-MaxVelocity, 0f, 0f);
            }

            if (Input.GetKey(KeyCode.UpArrow))
            {
                moveTo += new Vector3(0f, MaxVelocity, 0f);
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                moveTo += new Vector3(0f, -MaxVelocity, 0f);
            }
            
            if (moveTo == Vector3.zero)
            {
                MovingTowards = null;   
            }
            else
            {
                MovingTowards = transform.position + moveTo;
            }
        }
    }
}

