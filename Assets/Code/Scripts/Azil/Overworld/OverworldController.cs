﻿//using System.Linq;
//using System.Collections.Generic;
//using Azil.Battle;
//using Azil.GameState;
//using Azil.Utilities;
//using Azil.Characters;
//using Azil.Transitions;
//using UnityEngine;
//using UnityEngine.SceneManagement;

//namespace Azil.Overworld
//{
//    public class OverworldController : MonoBehaviour
//    {
//        public static OverworldController Main { get; private set; }
//        public GameObject PlayerPrefab;
//        public GameObject FollowerPrefab;
//        public GameObject PlayerInstance { get; protected set; }
//        public readonly List<GameObject> FollowerInstances = new List<GameObject>();
        
//        protected virtual void Awake()
//        {
//            if (Main == null)
//            {
//                Main = this;
//            }

//            AdjustPartyCharacters();
//        }

//        protected virtual void Update()
//        {
//            // TODO: this is bad
//            List<OverworldCharacter> sortedCharacters = FindObjectsOfType<OverworldCharacter>().ToList();
//            sortedCharacters.Sort((x, y) => x.GetComponent<SpriteRenderer>().bounds.min.y.CompareTo(y.GetComponent<SpriteRenderer>().bounds.min.y));
//            for (var i = 0; i < sortedCharacters.Count; i++)
//            {
//                OverworldCharacter overworldCharacter = sortedCharacters[i];
//                overworldCharacter.GetComponent<SpriteRenderer>().sortingOrder = -i;
//            }
//        }

//        public void AdjustPartyCharacters()
//        {
//            if (!SaveFile.IsLoaded() || SaveFile.LoadedSaveFile.Party.Count == 0) return;

//            if (PlayerInstance == null)
//            {
//                PlayerInstance = Instantiate(PlayerPrefab);
//            }

//            PlayerInstance.GetComponent<PlayerController>().Character = SaveFile.LoadedSaveFile.Party[0];
//            foreach (GameObject follower in FollowerInstances)
//            {
//                Destroy(follower);
//            }

//            for (int i = 1; i < SaveFile.LoadedSaveFile.Party.Count; i++)
//            {
//                Character partyMember = SaveFile.LoadedSaveFile.Party[i];
//                GameObject follower = Instantiate(FollowerPrefab);
//                follower.transform.position = PlayerInstance.transform.position;
//                follower.GetComponent<PlayerFollower>().Character = partyMember;

//                if (i == 1)
//                {
//                    follower.GetComponent<PlayerFollower>().FollowingCharacter = PlayerInstance.GetComponent<PartyCharacter>();
//                }
//                else if (i > 1)
//                {
//                    follower.GetComponent<PlayerFollower>().FollowingCharacter = FollowerInstances[i - 2].GetComponent<PartyCharacter>();
//                }

//                FollowerInstances.Add(follower);
//            }

//            //OverworldCameraController.Main
//        }

//        public float AddPartyMember(Character character)
//        {
//            //SaveFile.LoadedSaveFile.Party.Add(Instantiate(character));
//            Debug.Log($"{character.Name} has been added to the party!");

//            return 0f; // TODO: Eventually, needs to have flair, like in-game text and fanfare noise, all awaitable
//        }

//        public virtual void StartBattle(string scene, Transition transition, List<CharacterBase> otherBattlers)
//        {
//            Timing.RunCoroutine(DoLoad());

//            IEnumerator<float> DoLoad()
//            {
//                // Get the current scene, it will be unloaded later
//                Scene startScene = SceneManager.GetActiveScene();

//                // Start and wait on the specified scene transition effect
//                yield return transition.Start();

//                // Wait until the specified battle scene is loaded and make it the active one
//                yield return Timing.WaitUntilDone(SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive));
//                SceneManager.SetActiveScene(SceneManager.GetSceneByName(scene));

//                // Create instances of the other battlers that will be part of this battle
//                List<Character> instancedCharacters = new List<Character>();
//                foreach (CharacterBase template in otherBattlers)
//                {
//                    instancedCharacters.Add(new Character(template));
//                }

//                // Add the player's party and the other characters to a list, and start the battle with those characters
//                List<Character> characters = new List<Character>(SaveFile.LoadedSaveFile.Party);
//                characters.AddRange(instancedCharacters);
//                BattleController.Main.OverworldReturnScene = startScene.name;
//                BattleController.Main.SetupBattle(characters);

//                // Wait for the end of the transition
//                yield return transition.End();

//                // Wait until the overworld scene has unloaded
//                yield return Timing.WaitUntilDone(SceneManager.UnloadSceneAsync(startScene));
//            }
//        }
//    }
//}
