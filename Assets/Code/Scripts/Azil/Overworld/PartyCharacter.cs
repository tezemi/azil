﻿using Azil.Characters;

namespace Azil.Overworld
{
    public class PartyCharacter : OverworldCharacter
    {
        private Character _character;

        public Character Character
        {
            get
            {
                return _character;
            }
            set
            {
                if (value != null)
                {
                    //SpriteRenderer.sprite = value.CharactersSprites.Walking[0];
                }

                _character = value;
            }
        }
    }
}

