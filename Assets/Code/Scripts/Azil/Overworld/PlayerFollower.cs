﻿using UnityEngine;

namespace Azil.Overworld
{
    public class PlayerFollower : PartyCharacter
    {
        public PartyCharacter FollowingCharacter { get; set; }
        public const float DistanceBehind = 2f;
        public const float VerticalOffset = 0.15f;

        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            if (FollowingCharacter != null)
            {
                if (Vector2.Distance(FollowingCharacter.transform.position, transform.position) > DistanceBehind)
                {
                    MovingTowards = FollowingCharacter.transform.position + new Vector3(0f, VerticalOffset, 0f);
                }
                else
                {
                    MovingTowards = null;
                }
            }
        }
    }
}

