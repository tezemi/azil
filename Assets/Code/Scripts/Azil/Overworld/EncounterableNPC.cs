﻿using System.Collections.Generic;
using Azil.Characters;
using UnityEngine;

namespace Azil.Overworld
{
    public class EncounterableNPC : OverworldCharacter
    {
        public string BattleScene;
        public List<CharacterBase> Party;
        public bool Triggered { get; set; }
        protected BoxCollider2D TriggerBoxCollider { get; set; }

        protected override void Awake()
        {
            base.Awake();

            foreach (BoxCollider2D col in GetComponentsInChildren<BoxCollider2D>())
            {
                if (col.isTrigger)
                {
                    TriggerBoxCollider = col;
                    break;
                }
            }
        }

        protected virtual void OnEnable()
        {
            TriggerBoxCollider.size = new Vector2(SpriteRenderer.bounds.size.x, TriggerHeight);
            TriggerBoxCollider.offset = new Vector2(0f, -SpriteRenderer.bounds.size.y / 2f + TriggerHeight / 2f);
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (!Triggered && other.gameObject == PlayerController.Main.gameObject)
            {
                Triggered = true;
                PlayerController.Main.enabled = false;
                PlayerController.Main.MovingTowards = null;
                //OverworldController.Main.StartBattle(BattleScene, Transition, Party);
            }
        }
    }
}

