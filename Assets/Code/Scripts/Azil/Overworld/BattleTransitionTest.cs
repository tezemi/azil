﻿using UnityEngine;
using MyBox;
using Azil.Characters;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using MEC;
using Azil.UI.Transitions;
using Azil.UI.Overworld;
using Azil.Battles;

namespace Azil.Overworld
{
    public class BattleTransitionTest : MonoBehaviour
    {
        public BattleTransition[] BattleTransitions;
        public OverworldCanvas OverworldCanvas;

        public RenderTexture RenderTexture;

        public Camera Camera;

        [Scene]
        public string BattleScene;

        public CharacterBase[] DebugBattleCharacters;

        [ButtonMethod]
        public void StartBattle()
        {
            Timing.RunCoroutine(StartBattleCoroutine(), Segment.FixedUpdate);

            IEnumerator<float> StartBattleCoroutine()
            {
                Camera.GetComponent<AudioListener>().enabled = false;

                yield return Timing.WaitUntilDone(SceneManager.LoadSceneAsync(BattleScene, LoadSceneMode.Additive));

                var battleScene = FindObjectOfType<BattleScene>();

                var characters = new List<Character>();
                foreach (var characterBase in DebugBattleCharacters)
                {
                    characters.Add(new Character(characterBase));
                }

                var battle = battleScene.StartBattle(characters.ToArray());

                battleScene.BattleCamera.Camera.targetTexture = RenderTexture;

                yield return Timing.WaitUntilDone(OverworldCanvas.BattleTransitionCanvas.Transition(BattleTransitions.GetRandom()));

                yield return Timing.WaitUntilDone(SceneManager.UnloadSceneAsync(gameObject.scene.buildIndex));

                battleScene.BattleCamera.Camera.targetTexture = null;
            }
        }
    }
}
