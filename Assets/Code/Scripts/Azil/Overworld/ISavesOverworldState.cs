﻿
namespace Azil.Overworld
{
    public interface ISavesOverworldState<T>
    {
        int OverworldID { get; }
        T GetState();
        void SetState(T state);
    }
}
