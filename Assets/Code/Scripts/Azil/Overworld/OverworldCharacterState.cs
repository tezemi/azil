﻿using UnityEngine;

namespace Azil.Overworld
{
    public class OverworldCharacterState
    {
        public int ID { get; set; }
        public Vector3 Position { get; set; }

        public OverworldCharacterState(int iD, Vector3 position)
        {
            ID = iD;
            Position = position;
        }
    }
}
