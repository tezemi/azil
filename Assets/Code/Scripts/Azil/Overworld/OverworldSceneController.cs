﻿using UnityEngine;

namespace Azil.Overworld
{
    public class OverworldSceneController : MonoBehaviour
    {
        public static OverworldSceneController Main { get; private set; }
        public float DepthAdjustment;
        public float DepthMultiplier = 1f;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }
    }
}
