﻿using System.Collections.Generic;
using UnityEngine;
using MEC;

namespace Azil.Overworld
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class OverworldCharacter : MonoBehaviour, ISavesOverworldState<OverworldCharacterState>
    {
        public int SceneID = -1;
        public float MaxVelocity = 75f;
        public Vector3? MovingTowards { get; set; }
        public int OverworldID => SceneID;
        protected const float TriggerHeight = 0.25f;
        protected const float CollisionHeight = 0.15f;
        protected Rigidbody2D Rigidbody { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected BoxCollider2D SolidBoxCollider { get; set; }
        private Sprite _sprite;

        protected virtual void Awake()
        {
            Rigidbody = GetComponent<Rigidbody2D>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            foreach (BoxCollider2D col in GetComponents<BoxCollider2D>())
            {
                if (!col.isTrigger)
                {
                    SolidBoxCollider = col;
                    break;
                }
            }
        }

        protected virtual void Update()
        {
            if (_sprite != SpriteRenderer.sprite)
            {
                SolidBoxCollider.size = new Vector2(SpriteRenderer.bounds.size.x, CollisionHeight);
                SolidBoxCollider.offset = new Vector2(0f, -SpriteRenderer.bounds.size.y / 2f + CollisionHeight / 2f);
                _sprite = SpriteRenderer.sprite;
            }
        }

        protected virtual void FixedUpdate()
        {
            if (MovingTowards.HasValue)
            {
                Vector3 speed = (MovingTowards.Value - transform.position).normalized * MaxVelocity;
                Rigidbody.AddForce(speed);
            }
        }

        public virtual void MoveTowards(Vector3 vector3)
        {
            //MovingTowards = vector3;
            //Timing.WaitUntilDone(Timing.RunCoroutine(MoveUntilReached(), Segment.FixedUpdate, GetInstanceID().ToString()));

            //IEnumerator<float> MoveUntilReached()
            //{
            //    Vector3 localPoint = vector3;
            //    //yield return Timing.WaitUntilDone(() => transform.position == MovingTowards || localPoint != MovingTowards);
            //    MovingTowards = null;
            //}
        }
        
        public OverworldCharacterState GetState()
        {
            return new OverworldCharacterState(OverworldID, transform.position);
        }

        public void SetState(OverworldCharacterState state)
        {
            transform.position = state.Position;
        }
    }
}

